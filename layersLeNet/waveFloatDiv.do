onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /float_div_tb/DATA_WIDTH
add wave -noupdate /float_div_tb/LATENCY
add wave -noupdate /float_div_tb/rst_tb
add wave -noupdate /float_div_tb/clk_tb
add wave -noupdate /float_div_tb/syn_clr_tb
add wave -noupdate /float_div_tb/strobe_tb
add wave -noupdate -radix float32 /float_div_tb/dataa_tb
add wave -noupdate -radix float32 /float_div_tb/datab_tb
add wave -noupdate -radix float32 /float_div_tb/result_tb
add wave -noupdate /float_div_tb/data_ready_tb
add wave -noupdate /float_div_tb/busy_tb
add wave -noupdate /float_div_tb/DUT/altfp_div/aclr
add wave -noupdate /float_div_tb/DUT/altfp_div/clk_en
add wave -noupdate /float_div_tb/DUT/altfp_div/clock
add wave -noupdate -radix float32 /float_div_tb/DUT/altfp_div/dataa
add wave -noupdate -radix float32 /float_div_tb/DUT/altfp_div/datab
add wave -noupdate -radix float32 /float_div_tb/DUT/altfp_div/result
add wave -noupdate -radix float32 /float_div_tb/DUT/altfp_div/sub_wire0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {103511 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 310
configure wave -valuecolwidth 85
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {210 ns}
