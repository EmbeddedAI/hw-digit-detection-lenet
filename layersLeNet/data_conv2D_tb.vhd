LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY data_conv2D_tb IS 
END ENTITY data_conv2D_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_data_conv2D OF data_conv2D_tb IS 
	--========================================
	--                CONSTANTS               
	--========================================
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			:	STD_LOGIC := '0';
	SIGNAL	syn_clr_tb		:	STD_LOGIC;
	SIGNAL	enable_tb		:	STD_LOGIC;
	SIGNAL	columns_tb		:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	rows_tb			:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	columns_out_tb		:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	rows_out_tb			:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	data_ready_tb	:	STD_LOGIC;
	SIGNAL	data_ready_kernel_tb	:	STD_LOGIC;
	SIGNAL	padding_tb		:	STD_LOGIC;
	SIGNAL	index_out_tb	:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	padding_sel_tb	:	STD_LOGIC;
BEGIN
	--========================================
	--             RUN 196010ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb 			<= '0' AFTER 10ns; -- reset is modeled
	clk_tb 			<= NOT clk_tb AFTER 5ns; -- time is modeled
	syn_clr_tb		<=	'0';
	enable_tb		<=	'1';
	columns_tb		<=	"0011100";
	rows_tb			<=	"0011100";
	padding_sel_tb		<=	'1';
	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.data_conv2D
	PORT	MAP	(	rst					=>	rst_tb,
						clk					=>	clk_tb,
						syn_clr				=>	syn_clr_tb,
						enable				=>	enable_tb,
						padding_sel			=>	padding_sel_tb,
						columns_in			=>	columns_tb,
						rows_in				=>	rows_tb,
						data_ready			=>	data_ready_tb,
						data_ready_kernel	=>	data_ready_kernel_tb,
						padding				=>	padding_tb,
						index_out			=>	index_out_tb,
						columns_out			=> columns_out_tb,
						rows_out				=>	rows_out_tb); -- port mapping

END ARCHITECTURE testbench_data_conv2D;