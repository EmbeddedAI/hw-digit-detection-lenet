LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY mtx_segmentation_test_tb IS 
END ENTITY mtx_segmentation_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_mtx_segmentation_test OF mtx_segmentation_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	enable_tb	:	STD_LOGIC;
	SIGNAL	check_tb		:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 20ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	enable_tb	<=	'1';

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.mtx_segmentation_test
	PORT	MAP	(	enable	=>	enable_tb,
						check		=>	check_tb); -- port mapping

END ARCHITECTURE testbench_mtx_segmentation_test;