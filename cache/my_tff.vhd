LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--------------------------------------
ENTITY my_tff IS
	PORT (	clk	:	IN		STD_LOGIC;
				rst	:	IN		STD_LOGIC;
				ena	:	IN		STD_LOGIC;
				q		:	OUT 	STD_LOGIC);
END my_tff;
--------------------------------------
ARCHITECTURE behaviour OF my_tff IS
	SIGNAL q_s	:	STD_LOGIC;
BEGIN 
	my_tff1: PROCESS(clk,ena,rst)
	BEGIN
		IF(rst = '1') THEN
			q_s <= '0';
		ELSIF (rising_edge(clk))THEN
			IF(ena = '1') THEN 
				q_s	<= NOT (q_s);
			END IF;
		END IF;
	END PROCESS;
	q <= q_s;
END ARCHITECTURE;