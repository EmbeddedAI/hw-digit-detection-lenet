LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK;
USE WORK.my_package.all;
--------------------------------------------------------
ENTITY float_add_sub_tb IS 
	GENERIC	(	DATA_WIDTH		:	INTEGER	:=	32;
					LATENCY			:	UNSIGNED(LATENCY_WIDTH-1 DOWNTO 0)	:=	FLOAT_ADD_LATENCY);	
END ENTITY float_add_sub_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_float_add_sub OF float_add_sub_tb IS 
	--SIGNALS		
	SIGNAL	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			: 	STD_LOGIC := '0';
	SIGNAL	add_sub_tb		: 	STD_LOGIC;
	SIGNAL	syn_clr_tb		:	STD_LOGIC;
	SIGNAL	strobe_tb		: 	STD_LOGIC;
	SIGNAL	dataa_tb			: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	datab_tb			: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	result_tb		: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_ready_tb	:	STD_LOGIC;
	SIGNAL	busy_tb			:	STD_LOGIC;	
BEGIN

	rst_tb 		<= '0' AFTER 20ns; -- reset is modeled
	clk_tb 		<= NOT clk_tb AFTER 10ns; -- time is modeled
	syn_clr_tb	<= '0';
	strobe_tb	<=	'0','1' AFTER 40ns,'0' AFTER 60ns;
	dataa_tb		<=	"00111111001010111000010100011111";
	datab_tb		<=	"00111111000011001100110011001101";
	add_sub_tb	<=	'1';
	
--	float_add_sub_testbench_vectors : PROCESS                                               
--	-- variable declarations                                     
--	BEGIN   
--		--test vectors
--		
--	WAIT;                                                       
--	END PROCESS float_add_sub_testbench_vectors; -- the process is finished

	DUT: ENTITY work.float_add_sub
	GENERIC	MAP	(	DATA_WIDTH	=>	DATA_WIDTH,
							LATENCY		=>	LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst_tb,
							clk			=> clk_tb,
							syn_clr		=> syn_clr_tb,
							strobe		=> strobe_tb,
							add_sub		=>	add_sub_tb,
							dataa			=> dataa_tb,
							datab			=> datab_tb,
							result		=> result_tb,
							data_ready	=> data_ready_tb,
							busy			=> busy_tb); -- port mapping	
END ARCHITECTURE testbench_float_add_sub;