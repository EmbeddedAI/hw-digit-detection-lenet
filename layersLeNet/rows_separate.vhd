LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY rows_separate IS 
	GENERIC 	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:= 3); -- Define Generic Parameters
	PORT		(	data_in			:	IN		STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
					selector			:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					data_out			:	OUT	STD_LOGIC_VECTOR(KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY rows_separate; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF rows_separate is 
BEGIN
	--========================================
	--                CIRCUIT               
	--========================================
	mux_separate: FOR i IN 0 TO KERNEL_SIZE-1 GENERATE
		mux_i:	ENTITY work.mux_generic
		GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
							DATA_WIDTH_IN	=>	KERNEL_SIZE*BITS_FLOAT,
							DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
		PORT	MAP	(	data_in			=>	data_in(KERNEL_SIZE*BITS_FLOAT*(i+1)-1 DOWNTO KERNEL_SIZE*BITS_FLOAT*i),
							sel				=>	selector,
							data_out			=>	data_out(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i)); -- port mapping
	END GENERATE;
END ARCHITECTURE rtl; -- Architecture