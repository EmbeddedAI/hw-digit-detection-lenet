LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY mux_generic_test IS 
	GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 2;
					DATA_WIDTH_IN	:	INTEGER	:=	128;
					DATA_WIDTH_OUT	:	INTEGER	:=	32); -- Define Generic Parameters
	PORT	(	selector		:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
				comparator	:	IN		STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0);
				check			:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY mux_generic_test; -- Entity 
--------------------------------------------------------
ARCHITECTURE test_mux_generic OF mux_generic_test is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	buffer_one	:	STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
	SIGNAL 	buffer_two	:	STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	buffer_one	<= "0100000011100000000000000000000001000000110000000000000000000000010000001010000000000000000000000100000010000000000000000000000001000000010000000000000000000000010000000000000000000000000000000011111110000000000000000000000000000000000000000000000000000000";

	--========================================
	--        TEST BLOCK INSTANTIATION        
	--========================================
	TEST_B: ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=>	DATA_WIDTH_IN,
						DATA_WIDTH_OUT	=>	DATA_WIDTH_OUT) -- generic mapping
	PORT	MAP	(	data_in			=>	buffer_one,
						sel				=>	selector,
						data_out			=>	buffer_two); -- port mapping

	--========================================
	--                 OUTPUT
	--========================================			
	check	<=	'1'	WHEN buffer_two = comparator ELSE	'0';
END ARCHITECTURE test_mux_generic; -- Architecture
