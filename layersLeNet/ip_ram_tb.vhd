LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY ip_ram_tb IS 
	GENERIC (	DATA_WIDTH	:	INTEGER	:=	4;  
					ADDR_WIDTH	:	INTEGER	:=	3);
END ENTITY ip_ram_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_ip_ram OF ip_ram_tb IS
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	clk_tb			:	STD_LOGIC := '0';
	SIGNAL	data_tb			:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	rdaddress_tb	:	STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
	SIGNAL	wraddress_tb	:	STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
	SIGNAL	wren_tb			:	STD_LOGIC;
	SIGNAL	q_tb				:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
BEGIN
	--========================================
	--                RUN 550ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	clk_tb <= NOT clk_tb AFTER 5ns; -- time is modeled
	
	ip_ram_testbench_vectors : PROCESS                                                                                 
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		wren_tb <= '1';
		wraddress_tb	<= "000";
		rdaddress_tb	<= "000";
		data_tb			<= "0000";
		WAIT FOR 10ns;
		---------------- vector 1 ----------------
		wren_tb <= '0';
		wraddress_tb	<= "ZZZ"; 
		rdaddress_tb	<= "000"; 
		data_tb			<= "ZZZZ";
		WAIT FOR 10ns;
		---------------- vector 2 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "001";
		rdaddress_tb	<= "001";
		data_tb			<= "0001";
		WAIT FOR 10ns;
		---------------- vector 3 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "010"; 
		rdaddress_tb	<= "001"; 
		data_tb			<= "0010"; 
		WAIT FOR 10ns;
		---------------- vector 4 ----------------
		wren_tb 			<= '1';
		wraddress_tb	<= "011"; 
		rdaddress_tb	<= "010"; 
		data_tb			<= "0011";
		WAIT FOR 10ns;
		---------------- vector 5 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "100"; 
		rdaddress_tb	<= "011"; 
		data_tb			<= "0100"; 
		WAIT FOR 10ns;
		---------------- vector 6 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "101"; 
		rdaddress_tb	<= "100"; 
		data_tb			<= "0101"; 
		WAIT FOR 10ns;
		---------------- vector 7 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "110";
		rdaddress_tb	<= "101";
		data_tb			<= "0110";
		WAIT FOR 10ns;
		---------------- vector 8 ----------------
		wren_tb 			<= '1'; 
		wraddress_tb	<= "111";
		rdaddress_tb	<= "110";
		data_tb			<= "0111";
		WAIT FOR 10ns;
		---------------- vector 9 ----------------
		wren_tb 			<= '0'; 
		wraddress_tb	<= "ZZZ";
		rdaddress_tb	<= "111";
		data_tb			<= "ZZZZ";
		WAIT FOR 10ns;
	WAIT;                                                       
	END PROCESS ip_ram_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	DATA_WIDTH,
						ADDR_WIDTH	=>	ADDR_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk_tb,
						data			=>	data_tb,
						rdaddress	=>	rdaddress_tb,
						wraddress	=>	wraddress_tb,
						wren			=>	wren_tb,
						q				=>	q_tb); -- port mapping

END ARCHITECTURE testbench_ip_ram;