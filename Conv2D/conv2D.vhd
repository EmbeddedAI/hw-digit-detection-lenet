LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
------------------- CHANNEL ----------------------------
ENTITY conv2D IS 
	PORT	(	rst						:	IN		STD_LOGIC;
				clk						:	IN		STD_LOGIC;
				syn_clr					:	IN		STD_LOGIC;
				strobe					:	IN		STD_LOGIC;
				setup_layer				:	IN		STD_LOGIC;
				filters					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				channels					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				rows						:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				columns					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				data_in					:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
				data_in_rdaddress		:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
				data_out					:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
				data_out_wraddress	:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
				data_out_wren			:	OUT	STD_LOGIC;
				data_ready				:	OUT	STD_LOGIC;
				new_rows					:	OUT	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				new_columns				:	OUT	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0)); -- Define I/O Ports
END ENTITY conv2D; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_conv2D OF conv2D is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT ONES_KERNEL : STD_LOGIC_VECTOR(799 DOWNTO 0) := (OTHERS => '1');
	
	CONSTANT ZEROS_COUNT_FILTERS	:	UNSIGNED(COUNT_FILTERS_CONV2D-1 DOWNTO 0)	:=	(OTHERS => '0');
	CONSTANT ZEROS_FILTERS	:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1 DOWNTO 0)	:=	(OTHERS => '0');
	CONSTANT	ZEROS_SELECTOR			:	UNSIGNED(SELECTOR_KERNEL_SIZE-1 DOWNTO 0)	:=	(OTHERS => '0');
	CONSTANT	ZEROS_COUNTER			:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1 DOWNTO 0) :=	(OTHERS => '0');
	CONSTANT	ZEROS_KERNEL			:	UNSIGNED(SELECTOR_KERNEL_CONV2D-1 DOWNTO 0) :=	(OTHERS => '0');
	CONSTANT	ZEROS_CHANNEL_I			:	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0) :=	(OTHERS => '0');
	CONSTANT	ZEROS_DATA			:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0) :=	(OTHERS => '0');
	--========================================
	--                 TYPES
	--========================================
	TYPE state IS (idle,charge_data_first,charge_data_first_two,start_dot_product,next_channel,next_filter,charge_data,chargue_channel,operation_dot_product,next_data,wait_finish,ready);
	--========================================
	--                 SIGNALS                
	--========================================	
	SIGNAL 	pr_state,nx_state						: 	state;
	SIGNAL 	enable_data_conv2D			:	STD_LOGIC;
	SIGNAL	syn_clr_s						:	STD_LOGIC;
	SIGNAL	data_ready_kernel_charge	:	STD_LOGIC;
	SIGNAL	padding							:	STD_LOGIC;
	SIGNAL	enable_data_register			:	STD_LOGIC;
	SIGNAL	enable_data_register_two			:	STD_LOGIC;
	SIGNAL	ena_sel,syn_clr_sel			:	STD_LOGIC;
	SIGNAL	ena_kernel,syn_clr_kernel	:	STD_LOGIC;
	SIGNAL	ena_kernel_base,syn_clr_kernel_base	:	STD_LOGIC;
	SIGNAL	ena_counter_filters,syn_clr_counter_filters	:	STD_LOGIC;
	SIGNAL	ena_counter_channels,syn_clr_counter_channels	:	STD_LOGIC;
	SIGNAL	ena_chi,syn_clr_chi	:	STD_LOGIC;
	SIGNAL	syn_clr_counter	:	STD_LOGIC;
	SIGNAL	strobe_dot_product			:	STD_LOGIC;
	SIGNAL	set_data_b_acc					:	STD_LOGIC;
	SIGNAL	strobe_acc						:	STD_LOGIC;
	SIGNAL	data_ready_acc					:	STD_LOGIC;
	SIGNAL	strobe_add_sub			:	STD_LOGIC;
	SIGNAL	add_sub_s			:	STD_LOGIC;
	SIGNAL	act_channel,act_acc			:	STD_LOGIC;			
	SIGNAL	data_ready_add_sub			:	STD_LOGIC;
	SIGNAL	max_tick_filters,max_tick_channels,max_tick_selector				:	STD_LOGIC;
	SIGNAL	data_ready_dot_product		:	STD_LOGIC;
	SIGNAL	data_ready_add_sub_s			:	STD_LOGIC;
	SIGNAL	ena_dff							:	STD_LOGIC;
	SIGNAL	channel_i,channel_i_next	:	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	index_data,index_data_s						:	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	index_data_std,index_data_m	:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	index_data_channels			:	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	selector_one_hot_s				:	STD_LOGIC_VECTOR((2**SELECTOR_KERNEL_SIZE)-1 DOWNTO 0);
	SIGNAL	selector_one_hot				:	STD_LOGIC_VECTOR((2**SELECTOR_KERNEL_SIZE)-1 DOWNTO 0);
	SIGNAL	selector_two_hot				:	STD_LOGIC_VECTOR((2**SELECTOR_KERNEL_SIZE)-1 DOWNTO 0);
	SIGNAL	selector							:	STD_LOGIC_VECTOR(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
	SIGNAL	selector_s,selector_next	:	UNSIGNED(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
	SIGNAL	counter_s,counter_next		:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1 DOWNTO 0);
	SIGNAL	counter_channels,counter_channels_next		:	UNSIGNED(BITS_SETUP_CONV2D-1 DOWNTO 0);
	SIGNAL	counter_filters,counter_filters_next		:	UNSIGNED(BITS_SETUP_CONV2D-1 DOWNTO 0);
	SIGNAL	counter_filters_std		:	STD_LOGIC_VECTOR(BITS_SETUP_CONV2D-1 DOWNTO 0);
	SIGNAL	sel_kernel											:	STD_LOGIC_VECTOR((2*BITS_SETUP_CONV2D)-1 DOWNTO 0);
	SIGNAL	data_slice_kernel				:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_slice_kernel_s			:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	kernel_s							:	STD_LOGIC_VECTOR(FILTERS_CONV2D_TWO*CHANNELS_CONV2D_TWO*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	kernel							:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	bias_s								:	STD_LOGIC_VECTOR(FILTERS_CONV2D_TWO*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	bias								:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	result_acc,data_add_sub,result_dot	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data								:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	index_data_kernel				:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
BEGIN
	--========================================
	--                CIRCUIT               
	--========================================
	set_data_b_acc	<=	'0';
	--========================================
	--              DATA CONV2D
	--========================================
	data_conv2D: ENTITY work.data_conv2D
	PORT	MAP	(	rst					=>	rst,
						clk					=>	clk,
						syn_clr				=>	syn_clr_s,
						enable				=>	enable_data_conv2D,
						padding_sel			=> PADDING_SEL_CONV2D,
						columns_in			=>	columns,
						rows_in				=>	rows,
						data_ready			=>	OPEN,
						data_ready_kernel	=>	data_ready_kernel_charge,
						padding				=>	padding,
						index_out			=>	index_data,
						columns_out			=>	new_columns,
						rows_out				=>	new_rows); -- port mapping
						
	data_in_rdaddress	<=	STD_LOGIC_VECTOR(index_data_s)	WHEN	act_channel='1'	ELSE	
								STD_LOGIC_VECTOR(index_data);
	
	--========================================
	--              SUM SELECTOR
	--========================================
	selector_next	<=	ZEROS_SELECTOR					WHEN	syn_clr_sel='1'		ELSE
							selector_s+1					WHEN	ena_sel='1'				ELSE
							selector_s;
							
	max_tick_selector	<=	'1'	WHEN to_integer(selector_s)=KERNEL_SIZE_2-1 ELSE
								'0';

	--========================================
	--             CODER ONE HOT
	--========================================
	code_one_hot: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	SELECTOR_KERNEL_SIZE) -- generic mapping
	PORT	MAP	(	sel	=>	selector,
						f		=>	selector_one_hot_s); -- port mapping
						
	--========================================
	--               MUX PADDING
	--========================================
	mux_padding:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	data_in, -- 0
						x2					=>	ZEROS_DATA, -- 1
						sel				=>	padding,
						y					=>	data); -- port mapping

	--========================================
	--              SUM CHANNEL I
	--========================================
	channel_i_next	<=	ZEROS_CHANNEL_I		WHEN	syn_clr_chi='1'		ELSE
							channel_i+1				WHEN	ena_chi='1'				ELSE
							channel_i;
	
	--========================================
	--            REGISTERS KERNEL
	--========================================
	index_data_std	<=	STD_LOGIC_VECTOR(index_data);
	kernels_registers:	FOR i IN 0 TO KERNEL_SIZE*KERNEL_SIZE-1	GENERATE -- Last rows in zeros
		selector_one_hot(i)	<=	selector_one_hot_s(i) AND enable_data_register;
		selector_two_hot(i)	<=	selector_one_hot_s(i) AND enable_data_register_two;
		--========================================
		--               MUX CHANNEL
		--========================================
		my_reg_index_i: ENTITY  work.my_reg
		GENERIC MAP	(	DATA_WIDTH 	=> ADDR_BUFFERS_WIDTH)
		PORT MAP		(	clk			=>	clk,
							rst			=>	rst,
							ena			=>	selector_one_hot(i),
							syn_clr		=>	syn_clr_s,
							d				=>	index_data_std,
							q				=> index_data_kernel(ADDR_BUFFERS_WIDTH*(i+1)-1 DOWNTO ADDR_BUFFERS_WIDTH*i));
		my_reg_kernel_one_i: ENTITY  work.my_reg
		GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT)	
		PORT MAP		(	clk			=>	clk,
							rst			=>	rst,
							ena			=>	selector_one_hot(i),
							syn_clr		=>	syn_clr_s,
							d				=>	data,
							q				=> data_slice_kernel_s(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i));
		my_reg_kernel_two_i: ENTITY  work.my_reg
		GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT)		
		PORT MAP		(	clk			=>	clk,
							rst			=>	rst,
							ena			=>	selector_two_hot(i),
							syn_clr		=>	syn_clr_s,
							d				=>	data_slice_kernel_s(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							q				=> data_slice_kernel(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i));
	END GENERATE;
	
	--========================================
	--            MUX INDEX DATA
	--========================================
	mux_index_data: ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	SELECTOR_KERNEL_SIZE,
						DATA_WIDTH_IN	=>	KERNEL_SIZE*KERNEL_SIZE*ADDR_BUFFERS_WIDTH,
						DATA_WIDTH_OUT	=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	data_in			=>	index_data_kernel,
						sel				=>	selector,
						data_out			=>	index_data_m); -- port mapping
	index_data_s	<=	UNSIGNED(index_data_m)+channel_i;
	--========================================
	--              	COUNTER
	--========================================
	counter_next	<=	ZEROS_COUNTER	WHEN	syn_clr_counter='1'	ELSE
							counter_s+1		WHEN	data_ready_add_sub='1'		ELSE
							counter_s;
							
	data_out_wraddress	<=	STD_LOGIC_VECTOR(counter_s);
	
	--========================================
	--              INDEX KERNEL
	--========================================
	counter_channels_next		<=	ZEROS_COUNT_FILTERS	WHEN	syn_clr_counter_channels='1'		ELSE
											counter_channels+1	WHEN	ena_counter_channels='1'			ELSE
											counter_channels;
											
	max_tick_channels				<=	'1'	WHEN	counter_channels=channels-1	ELSE	'0';
											
	counter_filters_next			<=	ZEROS_COUNT_FILTERS								WHEN	syn_clr_counter_filters='1'	ELSE
											counter_filters+1									WHEN	ena_counter_filters='1'			ELSE
											counter_filters;
											
	max_tick_filters				<=	'1'	WHEN	counter_filters=filters-1	ELSE	'0';
	
	--========================================
	--           MUX KERNEL CONV2D
	--========================================
	mux_kernel_conv2D:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	FILTERS_CONV2D_TWO*CHANNELS_CONV2D_TWO*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	KERNEL_CONV2D_ONE, -- 0
						x2					=>	KERNEL_CONV2D_TWO, -- 1
						sel				=>	setup_layer,
						y					=>	kernel_s); -- port mapping
	
	--========================================
	--           MUX BIAS CONV2D
	--========================================
	mux_bias_conv2D:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	FILTERS_CONV2D_TWO*BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	BIAS_CONV2D_ONE, -- 0
						x2					=>	BIAS_CONV2D_TWO, -- 1
						sel				=>	setup_layer,
						y					=>	bias_s); -- port mapping
	
	sel_kernel	<=	STD_LOGIC_VECTOR(counter_filters*channels(BITS_SETUP_CONV2D-1 DOWNTO 0)+counter_channels);
	--========================================
	--            MUX KERNEL
	--========================================
--	mux_kernel: ENTITY work.mux_generic
--	GENERIC	MAP(	SIZE_SELECTOR	=>	2*BITS_SETUP_CONV2D,
--						DATA_WIDTH_IN	=>	FILTERS_CONV2D_TWO*CHANNELS_CONV2D_TWO*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT,
--						DATA_WIDTH_OUT	=>	KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	data_in			=>	kernel_s,
--						sel				=>	sel_kernel,
--						data_out			=>	kernel); -- port mapping
	kernel	<=	ONES_KERNEL;

	--========================================
	--            MUX BIAS
	--========================================
	counter_filters_std	<=	STD_LOGIC_VECTOR(counter_filters);
	mux_bias: ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	BITS_SETUP_CONV2D,
						DATA_WIDTH_IN	=>	FILTERS_CONV2D_TWO*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	bias_s,
						sel				=>	counter_filters_std,
						data_out			=>	bias); -- port mapping
				
	--========================================
	--              DOT PRODUCT
	--========================================
	dot_product: ENTITY work.dot_product
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
						SIZE_SELECTOR	=>	SELECTOR_ROWS_KERNEL) -- generic mapping
	PORT	MAP	(	rst				=>	rst,
						clk				=>	clk,
						syn_clr			=>	syn_clr_s,
						strobe			=>	strobe_dot_product,
						slice_mtx		=>	data_slice_kernel,
						kernel			=>	kernel,
						result_dot		=>	result_dot,
						data_ready		=>	data_ready_dot_product,
						busy				=>	OPEN); -- port mapping
						
	--========================================
	--              ACCUMULATOR
	--========================================
	act_acc		<=	'0'	WHEN to_integer(channels)=1	ELSE	'1';
	strobe_acc	<=	act_acc AND data_ready_dot_product;
	acc: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst,
						clk				=> clk,
						syn_clr			=> syn_clr_s,
						set_data_b		=> set_data_b_acc,
						strobe			=> strobe_acc,
						data_a			=>	result_dot,
						data_b			=> result_dot,
						result_acc		=> result_acc,
						data_ready		=> data_ready_acc,
						busy				=>	OPEN); -- port mapping
						
	--========================================
	--           MUX DATAA ADD_SUB
	--========================================
	mux_conv2D:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	result_dot, -- 0
						x2					=>	result_acc, -- 1
						sel				=>	act_acc,
						y					=>	data_add_sub); -- port mapping

	--========================================
	--        ADDER SUBSTRACTOR FLOAT
	--========================================
	add_sub_s	<=	'1';
	strobe_add_sub	<= (act_acc	AND data_ready_acc) OR (NOT act_acc AND data_ready_dot_product);
	add_sub: ENTITY work.float_add_sub
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr_s,
							strobe		=> strobe_add_sub,
							add_sub		=>	add_sub_s,
							dataa			=> data_add_sub,
							datab			=> bias,
							result		=> data_out,
							data_ready	=> data_ready_add_sub,
							busy			=>	OPEN); -- port mapping
	
	data_out_wren	<=	data_ready_add_sub;

	--========================================
	--           DATA READY ADD_SUB
	--========================================
	ena_dff	<=	'1';
	dff_data_ready: ENTITY  work.my_dff
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	ena_dff,
						d				=>	data_ready_add_sub,
						q				=> data_ready_add_sub_s);
						
	--===========================================
	--                    FSM
	--===========================================
	------------ Sequential Section -------------
	seq_fsm: PROCESS(clk,rst)
		VARIABLE temp_selector	:	UNSIGNED(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			pr_state 				<=	idle;
			temp_selector			:=	(OTHERS=>'0');
			counter_s				<=	(OTHERS=>'0');
			counter_channels		<=	(OTHERS=>'0');
			counter_filters		<=	(OTHERS=>'0');
			channel_i				<=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			IF	(syn_clr='1')	THEN
				pr_state 				<= idle;
			ELSE
				pr_state 				<= nx_state;
			END IF;
			temp_selector			:=	selector_next;
			counter_s				<=	counter_next;
			counter_channels		<=	counter_channels_next;
			counter_filters		<=	counter_filters_next;
			channel_i				<=	channel_i_next;
		END IF;
		selector		<=	STD_LOGIC_VECTOR(temp_selector);
		selector_s	<=	temp_selector;
	END PROCESS;
	----------- Combinational Section -----------
	comb_fsm: PROCESS (pr_state,strobe,act_acc,data_ready_kernel_charge,max_tick_selector,max_tick_filters,max_tick_channels,data_ready_dot_product,data_ready_add_sub_s)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				data_ready						<=	'0';
				syn_clr_s						<=	'1';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'1';
				syn_clr_counter				<=	'1';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'1';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'1';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				IF (strobe = '1') THEN
					nx_state	<= charge_data_first;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN charge_data_first =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'1';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'1';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				nx_state							<= charge_data_first_two;
			---------------------------
			WHEN charge_data_first_two =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'1';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				IF (data_ready_kernel_charge = '1') THEN
					nx_state	<= start_dot_product;
				ELSE
					nx_state	<= charge_data_first;
				END IF;
			---------------------------
			WHEN start_dot_product =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'1';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'1';
				act_channel						<=	'0';
				strobe_dot_product			<=	'1';
				IF (max_tick_channels= '1') THEN
					nx_state	<= next_filter;
				ELSIF (act_acc='1' AND max_tick_channels='0') THEN
					nx_state	<= next_channel;
				ELSE
					nx_state	<= charge_data;
				END IF;
			---------------------------
			WHEN next_filter =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'1';
				syn_clr_counter				<=	'1';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'1';
				ena_counter_filters			<=	'1';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				nx_state							<= charge_data;
			---------------------------
			WHEN next_channel =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'1';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'1';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				nx_state							<= chargue_channel;
			---------------------------
			WHEN charge_data =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'1';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'1';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'1';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				IF (data_ready_kernel_charge = '1') THEN
					nx_state	<= operation_dot_product;
				ELSE
					nx_state	<= charge_data;
				END IF;
			---------------------------
			WHEN chargue_channel =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'1';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'1';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'1';
				strobe_dot_product			<=	'0';
				IF (max_tick_selector = '1') THEN
					nx_state	<= operation_dot_product;
				ELSE
					nx_state	<= chargue_channel;
				END IF;
			---------------------------
			WHEN operation_dot_product =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				IF (data_ready_dot_product = '1' AND max_tick_channels='1' AND max_tick_filters='1') THEN
					nx_state	<= wait_finish;
				ELSIF (data_ready_dot_product = '1') THEN
					nx_state	<= next_data;
				ELSE
					nx_state	<= operation_dot_product;
				END IF;
			---------------------------
			WHEN next_data =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'1';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				nx_state							<= start_dot_product;
			---------------------------
			WHEN wait_finish =>
				data_ready						<=	'0';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'1';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'1';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				IF (data_ready_add_sub_s='1') THEN
					nx_state	<= ready;
				ELSE
					nx_state	<= wait_finish;
				END IF;
			---------------------------
			WHEN ready =>
				data_ready						<=	'1';
				syn_clr_s						<=	'0';
				ena_sel							<=	'0';
				syn_clr_sel						<=	'0';
				ena_chi							<=	'0';
				syn_clr_chi						<=	'0';
				syn_clr_counter				<=	'0';
				ena_counter_channels			<=	'0';
				syn_clr_counter_channels	<=	'0';
				ena_counter_filters			<=	'0';
				syn_clr_counter_filters		<=	'0';
				enable_data_register			<=	'0';
				enable_data_register_two	<=	'0';
				enable_data_conv2D			<=	'0';
				act_channel						<=	'0';
				strobe_dot_product			<=	'0';
				nx_state							<= idle;
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE arch_conv2D; -- Architecture