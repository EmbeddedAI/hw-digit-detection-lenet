onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_conv2d_tb/DUT/rst
add wave -noupdate /data_conv2d_tb/DUT/clk
add wave -noupdate /data_conv2d_tb/DUT/syn_clr
add wave -noupdate /data_conv2d_tb/DUT/enable
add wave -noupdate /data_conv2d_tb/DUT/padding_sel
add wave -noupdate -radix unsigned /data_conv2d_tb/DUT/columns_in
add wave -noupdate -radix unsigned /data_conv2d_tb/DUT/rows_in
add wave -noupdate /data_conv2d_tb/DUT/data_ready
add wave -noupdate /data_conv2d_tb/DUT/data_ready_kernel
add wave -noupdate /data_conv2d_tb/DUT/padding
add wave -noupdate /data_conv2d_tb/DUT/index_out
add wave -noupdate /data_conv2d_tb/DUT/index_data_k
add wave -noupdate -radix unsigned /data_conv2d_tb/DUT/columns_out
add wave -noupdate -radix unsigned /data_conv2d_tb/DUT/rows_out
add wave -noupdate /data_conv2d_tb/DUT/column_s
add wave -noupdate /data_conv2d_tb/DUT/column_next
add wave -noupdate /data_conv2d_tb/DUT/row_s
add wave -noupdate /data_conv2d_tb/DUT/row_next
add wave -noupdate /data_conv2d_tb/DUT/kernel_column_s
add wave -noupdate /data_conv2d_tb/DUT/kernel_column_next
add wave -noupdate /data_conv2d_tb/DUT/kernel_row_s
add wave -noupdate /data_conv2d_tb/DUT/kernel_row_next
add wave -noupdate /data_conv2d_tb/DUT/border_column
add wave -noupdate /data_conv2d_tb/DUT/border_row
add wave -noupdate /data_conv2d_tb/DUT/max_tick_column
add wave -noupdate /data_conv2d_tb/DUT/max_tick_row
add wave -noupdate /data_conv2d_tb/DUT/max_tick_kernel_column
add wave -noupdate /data_conv2d_tb/DUT/max_tick_kernel_row
add wave -noupdate /data_conv2d_tb/DUT/top_columns
add wave -noupdate -radix unsigned /data_conv2d_tb/DUT/columns
add wave -noupdate /data_conv2d_tb/DUT/rows
add wave -noupdate /data_conv2d_tb/DUT/top_rows
add wave -noupdate /data_conv2d_tb/DUT/index_out_s
add wave -noupdate /data_conv2d_tb/DUT/index_data_k_s
add wave -noupdate /data_conv2d_tb/DUT/ZEROS_COUNT
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {249261 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 414
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {249204 ps} {250042 ps}
