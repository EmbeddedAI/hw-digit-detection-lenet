LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY rows_separate_test_tb IS 
	GENERIC 	(	SIZE_SELECTOR	:	INTEGER	:= 3;
					KERNEL_SIZE		:	INTEGER	:=	5); -- Define Generic Parameters
END ENTITY rows_separate_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_rows_separate_test OF rows_separate_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	selector	:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	data_out	:	STD_LOGIC_VECTOR(KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_0	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_1	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_2	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_3	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_4	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
BEGIN
	--========================================
	--                RUN 100ns
	--========================================
	rows_separate_test_testbench_vectors : PROCESS                                               
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		selector	<=	"000";
		WAIT FOR 20ns;
		---------------- vector 1 ----------------
		selector	<=	"001";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		selector	<=	"010";
		WAIT FOR 20ns;
		---------------- vector 3 ----------------
		selector	<=	"011";
		WAIT FOR 20ns;
		---------------- vector 4 ----------------
		selector	<=	"100";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS rows_separate_test_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.rows_separate_test
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						KERNEL_SIZE		=>	KERNEL_SIZE) -- generic mapping
	PORT	MAP	(	selector			=>	selector,
						data_out			=>	data_out); -- port mapping

	--========================================
	--                 OUTPUT
	--========================================
	data_0	<=	data_out(1*BITS_FLOAT-1 DOWNTO 0);
	data_1	<=	data_out(2*BITS_FLOAT-1 DOWNTO 1*BITS_FLOAT);
	data_2	<=	data_out(3*BITS_FLOAT-1 DOWNTO 2*BITS_FLOAT);
	data_3	<=	data_out(4*BITS_FLOAT-1 DOWNTO 3*BITS_FLOAT);
	data_4	<=	data_out(5*BITS_FLOAT-1 DOWNTO 4*BITS_FLOAT);

END ARCHITECTURE testbench_rows_separate_test;