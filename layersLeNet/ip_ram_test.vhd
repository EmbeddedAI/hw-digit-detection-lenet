LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY ip_ram_test IS 
	GENERIC (	DATA_WIDTH	:	INTEGER	:=	25088;  
					ADDR_WIDTH	:	INTEGER	:=	6);
	PORT		(	enable		:	IN		STD_LOGIC;
					check			:	OUT	STD_LOGIC;
					clk			:	IN		STD_LOGIC;
					rdaddress	:	IN		STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
					wraddress	:	IN		STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
					wren			:	IN		STD_LOGIC); -- Define I/O Ports
END ENTITY ip_ram_test; 
--------------------------------------------------------
ARCHITECTURE test_ip_ram OF ip_ram_test IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data			:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	q				:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	comparator	:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
BEGIN
	--========================================
	--                 CIRCUIT
	--========================================
	--========================================
	--        TEST BLOCK INSTANTIATION        
	--========================================
	TEST_B: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	DATA_WIDTH,
						ADDR_WIDTH	=>	ADDR_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk,
						data			=>	data,
						rdaddress	=>	rdaddress,
						wraddress	=>	wraddress,
						wren			=>	wren,
						q				=>	q); -- port mapping

	--========================================
	--                 OUTPUT
	--========================================
	check	<=	'1'	WHEN	comparator=q AND enable='1'	ELSE	'0';
END ARCHITECTURE test_ip_ram;
