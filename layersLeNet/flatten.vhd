LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY flatten IS 
	GENERIC	(	CHANNELS	:	INTEGER	:=	50;
					COLUMNS	:	INTEGER	:=	7;
					ROWS		:	INTEGER	:=	7); -- Define Generic Parameters
	PORT		(	data_in	:	IN		STD_LOGIC_VECTOR(CHANNELS*COLUMNS*ROWS*BITS_FLOAT-1 DOWNTO 0);
					data_out	:	OUT	STD_LOGIC_VECTOR(CHANNELS*COLUMNS*ROWS*BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY flatten; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_flatten OF flatten is 
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	channels_i:	FOR channelPosition IN 0 TO CHANNELS-1	GENERATE
		columns_i:	FOR columnPosition IN 0 TO COLUMNS-1	GENERATE
			rows_i:	FOR rowPosition IN 0 TO ROWS-1	GENERATE
				data_out(((rowPosition*ROWS*CHANNELS+channelPosition*ROWS+columnPosition)+1)*BITS_FLOAT-1 DOWNTO (rowPosition*ROWS*CHANNELS+channelPosition*ROWS+columnPosition)*BITS_FLOAT)	<=	data_in(((channelPosition*ROWS*COLUMNS+columnPosition*COLUMNS+rowPosition)+1)*BITS_FLOAT-1 DOWNTO (channelPosition*ROWS*COLUMNS+columnPosition*COLUMNS+rowPosition)*BITS_FLOAT);
			END GENERATE; -- END ROWS
		END GENERATE; -- END COLUMNS
	END GENERATE; -- END CHANNELS
END ARCHITECTURE arch_flatten; -- Architecture