LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--------------------------------------------------------------
ENTITY coder_one_hot IS 
	GENERIC(	N			:	INTEGER := 8);
	PORT(		sel		:	IN		STD_LOGIC_VECTOR(N-1 DOWNTO 0);
				f			:	OUT	STD_LOGIC_VECTOR((2**N)-1 DOWNTO 0));
END ENTITY coder_one_hot;
------------------------------------------------------------------
ARCHITECTURE arch_coder_one_hot OF coder_one_hot	IS
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	one_hot_code: FOR i IN 0 TO (2**N)-1 GENERATE	
		f(i)	<= '1'	WHEN sel = STD_LOGIC_VECTOR(to_unsigned(i,N)) ELSE
					'0';
	END GENERATE;
END ARCHITECTURE arch_coder_one_hot;