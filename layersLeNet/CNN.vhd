LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY CNN IS 
	PORT		(	rst			:	IN		STD_LOGIC;
					clk			:	IN		STD_LOGIC;
					syn_clr		:	IN		STD_LOGIC;
					charge_data	:	IN		STD_LOGIC;
					finish_data	:	IN		STD_LOGIC;
					strobe		:	IN		STD_LOGIC;
					take_data	:	IN		STD_LOGIC;
					data_ena		:	IN		STD_LOGIC;
					address_in	:	IN		STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
					data_in		:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
					data_out		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
					data_ready	:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY CNN; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_CNN OF CNN is 
	--========================================
	--                CONSTANTS               
	--========================================
	--========================================
	--                 TYPES
	--========================================
	TYPE state IS (idle,recieve_data_input,start_layer,operating_layer,ready_layer,send_data_output);
	--========================================
	--                 SIGNALS                
	--========================================
		SIGNAL 	pr_state,nx_state					: 	state;
	SIGNAL	selector_buffer						:	STD_LOGIC;	
	SIGNAL	wr_en_buffer_input					:	STD_LOGIC;
	
	SIGNAL	buffer_internal_data					:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_one_data						:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_two_data						:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
--	SIGNAL	buffer_input_data						:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_output_data					:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	kernel_data								:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	
	SIGNAL	buffer_output_data_layers			:	STD_LOGIC_VECTOR(NUM_LAYERS*BITS_FLOAT-1	DOWNTO 0);
	
	SIGNAL	buffer_internal_rdaddress			:	STD_LOGIC_VECTOR(ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_internal_rdaddress_layers	:	STD_LOGIC_VECTOR(NUM_LAYERS*ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_one_rdaddress					:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_two_rdaddress					:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_input_rdaddress				:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	kernel_rdaddress						:	STD_LOGIC_VECTOR(ADDR_KERNEL_WIDTH-1	DOWNTO 0);
	
	SIGNAL	buffer_input_rdaddress_layers		:	STD_LOGIC_VECTOR(NUM_LAYERS*ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
--	SIGNAL	buffer_output_rdaddress				:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	
	
	SIGNAL	buffer_internal_wraddress			:	STD_LOGIC_VECTOR(ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_one_wraddress					:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_two_wraddress					:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
--	SIGNAL	buffer_input_wraddress				:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	buffer_output_wraddress				:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	kernel_wraddress						:	STD_LOGIC_VECTOR(ADDR_KERNEL_WIDTH-1	DOWNTO 0);
	
	SIGNAL	buffer_output_wraddress_layers	:	STD_LOGIC_VECTOR(NUM_LAYERS*ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	
	SIGNAL	buffer_internal_wren					:	STD_LOGIC;
	SIGNAL	buffer_one_wren						:	STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL	buffer_two_wren						:	STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL	buffer_input_wren						:	STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL	buffer_output_wren					:	STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL	kernel_wren								:	STD_LOGIC;
	SIGNAL	buffer_output_wren_layers			:	STD_LOGIC_VECTOR(NUM_LAYERS-1 DOWNTO 0);
	
	SIGNAL	buffer_internal_q						:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_one_q							:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_two_q							:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	buffer_input_q							:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	kernel_q									:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
--	SIGNAL	buffer_output_q						:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	
	SIGNAL	syn_clr_s								:	STD_LOGIC;
	SIGNAL	strobe_layer							:	STD_LOGIC;
	SIGNAL	strobe_layers							:	STD_LOGIC_VECTOR((2**NUM_LAYERS_SELECTOR)-1 DOWNTO 0);
	SIGNAL	strobe_layers_s						:	STD_LOGIC_VECTOR((2**NUM_LAYERS_SELECTOR)-1 DOWNTO 0);
	SIGNAL	data_ready_layers						:	STD_LOGIC_VECTOR(NUM_LAYERS-1 DOWNTO 0);
	SIGNAL	selector_current_layer				:	STD_LOGIC_VECTOR(NUM_LAYERS_SELECTOR-1 DOWNTO 0);
	
	SIGNAL	conv2D_out,flatten_out				:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	dense_out,ReLU_out,ReLU_out_s		:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	maxpooling2D_out,softmax_out		:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	setup_layers							:	STD_LOGIC_VECTOR(NUM_LAYERS-1 DOWNTO 0);

	SIGNAL	padding								:	STD_LOGIC;
	SIGNAL	filters								:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	channels								:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	rows									:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	columns								:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	kernel_base							:	UNSIGNED(ADDR_KERNEL_WIDTH-1 DOWNTO 0);
	SIGNAL	bias_base							:	UNSIGNED(ADDR_KERNEL_WIDTH-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	selector_current_layer	<=	"101";
	selector_buffer			<= '0';
	buffer_input_wren(0)		<= wr_en_buffer_input AND data_ena;
	--========================================
	--                 CONV2D
	--========================================
	padding	<=	'1';
	filters	<=	"0010100";
	channels	<=	"0000001";
	rows		<=	"0011100";
	columns	<=	"0011100";
	kernel_base	<=	"000000000010100";
	bias_base	<=	"000000000000000";
	conv2D_layer:	ENTITY work.conv2D
	PORT	MAP	(	rst						=> rst,
						clk						=> clk,
						syn_clr					=> syn_clr_s,
						strobe					=> strobe_layers(ID_CONV2D),
						padding					=> padding,
						filters					=> filters,
						channels					=> channels,
						rows						=> rows,
						columns					=> columns,
						kernel_base				=> kernel_base,
						bias_base				=> bias_base,
						data_in					=> buffer_input_q,
						kernel_in				=>	kernel_q,
						data_in_rdaddress		=> buffer_input_rdaddress_layers((ID_CONV2D+1)*ADDR_BUFFERS_WIDTH-1 DOWNTO ID_CONV2D*ADDR_BUFFERS_WIDTH),
						kernel_in_rdaddress	=>	kernel_rdaddress,
						data_out					=> buffer_output_data_layers((ID_CONV2D+1)*BITS_FLOAT-1 DOWNTO ID_CONV2D*BITS_FLOAT),
						data_out_wraddress	=> buffer_internal_wraddress,
						data_out_wren			=> buffer_internal_wren,
						data_ready				=> data_ready_layers(ID_CONV2D),
						channels_out			=>	OPEN,
						rows_out					=>	OPEN,
						columns_out				=>	OPEN); -- port mapping
	--========================================
	--               MUX CONV2D
	--========================================
--	mux_conv2D:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	, -- 0
--						x2					=>	, -- 1
--						sel				=>	setup_layers(ID_CONV2D),
--						y					=>	conv2D_out); -- port mapping

	--========================================
	--                 FLATTEN
	--========================================
	--========================================
	--               MUX FLATTEN
	--========================================
--	mux_flatten:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	conv2D_out, -- 0
--						x2					=>	, -- 1
--						sel				=>	setup_layers(ID_FLATTEN),
--						y					=>	flatten_out); -- port mapping
--						
	--========================================
	--                  DENSE
	--========================================
	--========================================
	--               MUX DENSE
	--========================================
--	mux_flatten:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	flatten_out, -- 0
--						x2					=>	, -- 1
--						sel				=>	setup_layers(ID_DENSE),
--						y					=>	dense_out); -- port mapping
	             
	--========================================
	--                  RELU
	--========================================
	ReLU_layer:	ENTITY work.ReLU
	PORT	MAP	(	data_in	=>	dense_out,
						data_out	=>	ReLU_out_s); -- port mapping
	--========================================
	--                MUX RELU
	--========================================
--	mux_flatten:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	dense_out, -- 0
--						x2					=>	ReLU_out_s, -- 1
--						sel				=>	setup_layers(ID_RELU),
--						y					=>	ReLU_out); -- port mapping
	
	--========================================
	--              MAXPOOLING2D
	--========================================
	--========================================
	--            MUX MAXPOOLING2D
	--========================================
--	mux_flatten:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	, -- 0
--						x2					=>	, -- 1
--						sel				=>	setup_layers(ID_MAXPOOLING2D),
--						y					=>	maxpooling2D_out); -- port mapping

	--========================================
	--                 SOFTMAX
	--========================================
	softmax_layer:	ENTITY work.softmax
	PORT	MAP	(	rst						=> rst,
						clk						=> clk,
						syn_clr					=> syn_clr_s,
						strobe					=> strobe_layers(ID_SOFTMAX),
						data_in					=> buffer_input_q,
						data_in_rdaddress		=> buffer_input_rdaddress_layers((ID_SOFTMAX+1)*ADDR_BUFFERS_WIDTH-1 DOWNTO ID_SOFTMAX*ADDR_BUFFERS_WIDTH),
						data_out					=> buffer_output_data_layers((ID_SOFTMAX+1)*BITS_FLOAT-1 DOWNTO ID_SOFTMAX*BITS_FLOAT),
						data_out_wraddress	=> buffer_output_wraddress_layers((ID_SOFTMAX+1)*ADDR_BUFFERS_WIDTH-1 DOWNTO ID_SOFTMAX*ADDR_BUFFERS_WIDTH),
						data_out_wren			=> buffer_output_wren_layers(ID_SOFTMAX),
						data_ready				=> data_ready_layers(ID_SOFTMAX)); -- port mapping
	--========================================
	--               MUX SOFTMAX
	--========================================
--	mux_flatten:	ENTITY work.mux2_1
--	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
--	PORT	MAP	(	x1					=>	, -- 0
--						x2					=>	, -- 1
--						sel				=>	setup_layers(ID_SOFTMAX),
--						y					=>	softmax_out); -- port mapping


	--========================================
	--              STROBE LAYERS
	--========================================
	-------------- CODER ONE HOT -------------
	code_one_hot: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	NUM_LAYERS_SELECTOR) -- generic mapping
	PORT	MAP	(	sel	=>	selector_current_layer,
						f		=>	strobe_layers_s); -- port mapping
	--------------- AND STROBE ---------------
	and_strobe:	FOR i IN 0 TO NUM_LAYERS-1 GENERATE 
		strobe_layers(i)	<=	strobe_layer AND strobe_layers_s(i);
	END GENERATE;
	
	--========================================
	--      CONTROL BUFFERS INPUT/OUTPUT
	--========================================
	------- MUX BUFFER INPUT RDADDRESS -------
	mux_buffer_input_rdaddress:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	NUM_LAYERS_SELECTOR,
						DATA_WIDTH_IN	=>	NUM_LAYERS*ADDR_BUFFERS_WIDTH,
						DATA_WIDTH_OUT	=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	data_in			=>	buffer_input_rdaddress_layers,
						sel				=>	selector_current_layer,
						data_out			=>	buffer_input_rdaddress); -- port mapping
	----------- MUX BUFFER OUT DATA ----------
	mux_buffer_output_data:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	NUM_LAYERS_SELECTOR,
						DATA_WIDTH_IN	=>	NUM_LAYERS*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	buffer_output_data_layers,
						sel				=>	selector_current_layer,
						data_out			=>	buffer_output_data); -- port mapping
	-------- MUX BUFFER OUT WRADDRESS --------
	mux_buffer_output_wraddress:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	NUM_LAYERS_SELECTOR,
						DATA_WIDTH_IN	=>	NUM_LAYERS*ADDR_BUFFERS_WIDTH,
						DATA_WIDTH_OUT	=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	data_in			=>	buffer_output_wraddress_layers,
						sel				=>	selector_current_layer,
						data_out			=>	buffer_output_wraddress); -- port mapping
	----------- MUX BUFFER OUT WREN ----------
	mux_buffer_output_wren:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	NUM_LAYERS_SELECTOR,
						DATA_WIDTH_IN	=>	NUM_LAYERS,
						DATA_WIDTH_OUT	=>	1) -- generic mapping
	PORT	MAP	(	data_in			=>	buffer_output_wren_layers,
						sel				=>	selector_current_layer,
						data_out			=>	buffer_output_wren); -- port mapping
	----------- CODER BUFFERS DATA -----------
	coder_buffer_data:	ENTITY work.coder_buffer
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	selector			=>	selector_buffer,
						input_one		=>	data_in,
						input_two		=>	buffer_output_data,
						output_one		=>	buffer_one_data,
						output_two		=>	buffer_two_data); -- port mapping
	--------- CODER BUFFERS RDADDRESS --------
	coder_buffer_rdaddress:	ENTITY work.coder_buffer
	GENERIC	MAP(	DATA_WIDTH		=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	selector			=>	selector_buffer,
						input_one		=>	buffer_input_rdaddress,
						input_two		=>	address_in,
						output_one		=>	buffer_one_rdaddress,
						output_two		=>	buffer_two_rdaddress); -- port mapping
	--------- CODER BUFFERS WRADDRESS --------
	coder_buffer_wraddres:	ENTITY work.coder_buffer
	GENERIC	MAP(	DATA_WIDTH		=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	selector			=>	selector_buffer,
						input_one		=>	address_in,
						input_two		=>	buffer_output_wraddress,
						output_one		=>	buffer_one_wraddress,
						output_two		=>	buffer_two_wraddress); -- port mapping
	----------- CODER BUFFERS WREN -----------
	coder_buffer_wren:	ENTITY work.coder_buffer
	GENERIC	MAP(	DATA_WIDTH		=>	1) -- generic mapping
	PORT	MAP	(	selector			=>	selector_buffer,
						input_one		=>	buffer_input_wren,
						input_two		=>	buffer_output_wren,
						output_one		=>	buffer_one_wren,
						output_two		=>	buffer_two_wren); -- port mapping
	------------- CODER BUFFERS Q ------------
	coder_buffer_q:	ENTITY work.coder_buffer
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	selector			=>	selector_buffer,
						input_one		=>	buffer_one_q,
						input_two		=>	buffer_two_q,
						output_one		=>	buffer_input_q,
						output_two		=>	data_out); -- port mapping
	
	--========================================
	--           RAM BUFFER INTERNAL
	--========================================
	buffer_internal: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	BITS_FLOAT,
						ADDR_WIDTH	=>	ADDR_BUFFER_INTERNAL_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk,
						data			=>	buffer_internal_data,
						rdaddress	=>	buffer_internal_rdaddress,
						wraddress	=>	buffer_internal_wraddress,
						wren			=>	buffer_internal_wren,
						q				=>	buffer_internal_q); -- port mapping

	--========================================
	--        RAM BUFFER 1 INPUT/OUTPUT
	--========================================
	buffer_one: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	BITS_FLOAT,
						ADDR_WIDTH	=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk,
						data			=>	buffer_one_data,
						rdaddress	=>	buffer_one_rdaddress,
						wraddress	=>	buffer_one_wraddress,
						wren			=>	buffer_one_wren(0),
						q				=>	buffer_one_q); -- port mapping

	--========================================
	--        RAM BUFFER 2 INPUT/OUTPUT
	--========================================
	buffer_two: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	BITS_FLOAT,
						ADDR_WIDTH	=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk,
						data			=>	buffer_two_data,
						rdaddress	=>	buffer_two_rdaddress,
						wraddress	=>	buffer_two_wraddress,
						wren			=>	buffer_two_wren(0),
						q				=>	buffer_two_q); -- port mapping

	--========================================
	--               RAM KERNEL
	--========================================
	ram_kernel: ENTITY work.ip_ram
	GENERIC	MAP(	DATA_WIDTH	=>	BITS_FLOAT,
						ADDR_WIDTH	=>	ADDR_KERNEL_WIDTH) -- generic mapping
	PORT	MAP	(	clock			=>	clk,
						data			=>	kernel_data,
						rdaddress	=>	kernel_rdaddress,
						wraddress	=>	kernel_wraddress,
						wren			=>	kernel_wren,
						q				=>	kernel_q); -- port mapping

	--===========================================
	--                    FSM
	--===========================================
	------------ Sequential Section -------------
	seq_fsm: PROCESS(clk,rst)
--		VARIABLE temp_selector	:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			pr_state 		<=	idle;
--			temp_selector	:=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			pr_state 		<= nx_state;
--			temp_selector	:=	selector_next;
		END IF;
--		selector		<=	STD_LOGIC_VECTOR(temp_selector);
--		selector_s	<=	temp_selector;
	END PROCESS;
	----------- Combinational Section -----------
	comb_fsm: PROCESS (pr_state,charge_data,finish_data,syn_clr,strobe,data_ready_layers,take_data,selector_current_layer)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				wr_en_buffer_input	<=	'0';
				strobe_layer			<= '0';
				data_ready				<=	'0';
				syn_clr_s				<= '1';
				IF (charge_data='1') THEN
					nx_state	<= recieve_data_input;
				ELSIF (strobe='1') THEN
					nx_state	<= start_layer;
				ELSIF (take_data='1') THEN
					nx_state	<= send_data_output;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN recieve_data_input =>
				wr_en_buffer_input	<=	'1';
				strobe_layer			<= '0';
				data_ready				<=	'0';
				syn_clr_s				<= '0';
				IF (syn_clr='1' OR finish_data='1') THEN
					nx_state	<= idle;
				ELSE
					nx_state	<= recieve_data_input;
				END IF;
			---------------------------
			WHEN start_layer =>
				wr_en_buffer_input	<=	'0';
				strobe_layer			<= '1';
				data_ready				<=	'0';
				syn_clr_s				<= '0';
				nx_state					<= operating_layer;
			---------------------------
			WHEN operating_layer =>
				wr_en_buffer_input	<=	'0';
				strobe_layer			<= '0';
				data_ready				<=	'0';
				syn_clr_s				<= '0';
				IF (syn_clr='1') THEN
					nx_state	<= idle;
				ELSIF (data_ready_layers(to_integer(UNSIGNED(selector_current_layer)))='1') THEN
					nx_state	<= ready_layer;
				ELSE
					nx_state	<= operating_layer;
				END IF;
			---------------------------
			WHEN ready_layer =>
				wr_en_buffer_input	<=	'0';
				strobe_layer			<= '0';
				data_ready				<=	'1';
				syn_clr_s				<= '0';
				nx_state					<= idle;
			---------------------------
			WHEN send_data_output =>
				wr_en_buffer_input	<=	'0';
				strobe_layer			<= '0';
				data_ready				<=	'0';
				syn_clr_s				<= '0';
				IF (syn_clr='1' OR finish_data='1') THEN
					nx_state	<= idle;
				ELSE
					nx_state	<= send_data_output;
				END IF;
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE arch_CNN; -- Architecture