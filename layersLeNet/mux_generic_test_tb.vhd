LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY mux_generic_test_tb IS 
GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 3;
				DATA_WIDTH_IN	:	INTEGER	:=	256;
				DATA_WIDTH_OUT	:	INTEGER	:=	32); -- Define Generic Parameters
END ENTITY mux_generic_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_mux_generic_test OF mux_generic_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	comparator_tb	:	STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0);
	SIGNAL 	selector_tb		:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	check_tb			:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 160ns
	--========================================
	mux_generic_test_testbench_vectors : PROCESS                                                                                 
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		selector_tb		<=	"000";
		comparator_tb	<=	"00000000000000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 1 ----------------
		selector_tb		<=	"001";
		comparator_tb	<=	"00111111100000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		selector_tb		<=	"010";
		comparator_tb	<=	"01000000000000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 3 ----------------
		selector_tb		<=	"011";
		comparator_tb	<=	"01000000010000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 4 ----------------
		selector_tb		<=	"100";
		comparator_tb	<=	"01000000100000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 5 ----------------
		selector_tb		<=	"101";
		comparator_tb	<=	"01000000101000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 6 ----------------
		selector_tb		<=	"110";
		comparator_tb	<=	"01000000110000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 7 ----------------
		selector_tb		<=	"111";
		comparator_tb	<=	"01000000111000000000000000000000";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS mux_generic_test_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.mux_generic_test
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=>	DATA_WIDTH_IN,
						DATA_WIDTH_OUT	=>	DATA_WIDTH_OUT) -- generic mapping
	PORT	MAP	(	selector			=>	selector_tb,
						comparator		=>	comparator_tb,
						check				=>	check_tb); -- port mapping

END ARCHITECTURE testbench_mux_generic_test;