onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /test_flatten_data_tb/DATA_WIDTH
add wave -noupdate /test_flatten_data_tb/DATA_QUANTY
add wave -noupdate /test_flatten_data_tb/SIZE_SELECTOR
add wave -noupdate /test_flatten_data_tb/rst_tb
add wave -noupdate /test_flatten_data_tb/clk_tb
add wave -noupdate /test_flatten_data_tb/syn_clr_tb
add wave -noupdate -radix unsigned /test_flatten_data_tb/selector_tb
add wave -noupdate /test_flatten_data_tb/enable_tb
add wave -noupdate -radix float32 /test_flatten_data_tb/data_in_tb
add wave -noupdate /test_flatten_data_tb/check_tb
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_0
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_1
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_2
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_3
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_4
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_5
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_6
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_7
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_8
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_9
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_10
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_11
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_12
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_13
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_14
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_15
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_16
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_17
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_18
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_19
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_20
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_21
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_22
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_23
add wave -noupdate -radix float32 /test_flatten_data_tb/DUT/data_24
add wave -noupdate /test_flatten_data_tb/DUT/fd/enable_registers_s
add wave -noupdate /test_flatten_data_tb/DUT/fd/enable_registers
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {207852 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 279
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
run 540ns
WaveRestoreZoom {0 ps} {512966 ps}
