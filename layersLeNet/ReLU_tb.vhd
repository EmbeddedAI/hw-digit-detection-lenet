LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY ReLU_tb IS 
END ENTITY ReLU_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_ReLU OF ReLU_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data_in_tb	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_out_tb	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
BEGIN
	--========================================
	--                RUN 60ns
	--========================================
	ReLU_testbench_vectors : PROCESS                                               
		--========================================
		--         VARIABLE DECLARATIONS
		--========================================                                     
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		data_in_tb	<= "01000001001010000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 1 ----------------
		data_in_tb	<= "00000000000000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		data_in_tb	<= "11000001001010000000000000000000";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS ReLU_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.ReLU
	PORT	MAP	(	data_in	=>	data_in_tb,
						data_out	=>	data_out_tb); -- port mapping

END ARCHITECTURE testbench_ReLU;