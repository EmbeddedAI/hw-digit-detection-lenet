LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
------------------- CHANNEL ----------------------------
ENTITY conv2D IS 
	PORT	(	rst						:	IN		STD_LOGIC;
				clk						:	IN		STD_LOGIC;
				syn_clr					:	IN		STD_LOGIC;
				strobe					:	IN		STD_LOGIC;
				padding					:	IN		STD_LOGIC;
				filters					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				channels					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				rows						:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				columns					:	IN		UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				kernel_base				:	IN		UNSIGNED(ADDR_KERNEL_WIDTH-1 DOWNTO 0);
				bias_base				:	IN		UNSIGNED(ADDR_KERNEL_WIDTH-1 DOWNTO 0);
				data_in					:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
				kernel_in				:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
				data_in_rdaddress		:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
				kernel_in_rdaddress	:	OUT	STD_LOGIC_VECTOR(ADDR_KERNEL_WIDTH-1 DOWNTO 0);
				data_out					:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
				data_out_wraddress	:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
				data_out_wren			:	OUT	STD_LOGIC;
				data_ready				:	OUT	STD_LOGIC;
				channels_out			:	OUT	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				rows_out					:	OUT	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
				columns_out				:	OUT	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0)); -- Define I/O Ports
END ENTITY conv2D; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_conv2D OF conv2D is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT ZEROS_COUNTERS									:	UNSIGNED(MAX_COUNTER_WIDTH-BITS_SETUP_CONV2D-2 DOWNTO 0)	:=	(OTHERS	=>	'0');
	CONSTANT ZEROS_DATA										:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0)						:=	(OTHERS	=>	'0');
	--========================================
	--                 TYPES
	--========================================
	TYPE state IS (idle,f_take_bias,f_take_data_kernel,f_data_kernel,start_dot,take_channel,take_bias,take_data_kernel,data_kernel,dot,wait_finish,ready);
	--========================================
	--                 SIGNALS                
	--========================================	
	SIGNAL 	pr_state,nx_state															: 	state;
	SIGNAL	syn_clr_s,flag_channels													:	STD_LOGIC;
	SIGNAL	enable_data_flatten														:	STD_LOGIC;
	SIGNAL	enable_register_data_flatten											:	STD_LOGIC;
--	SIGNAL	enable_kernel_flatten													:	STD_LOGIC;
	SIGNAL	enable_register_kernel_flatten										:	STD_LOGIC;
	SIGNAL	strobe_dot_product,data_ready_dot									:	STD_LOGIC;
	SIGNAL	set_data_b_acc																:	STD_LOGIC;
	SIGNAL	strobe_acc,data_ready_acc												:	STD_LOGIC;
	SIGNAL	strobe_add_sub,add_sub_s												:	STD_LOGIC;
	SIGNAL	data_ready_add_sub														:	STD_LOGIC;
	SIGNAL	enable_register_bias														:	STD_LOGIC;
	
	SIGNAL	sel_kernel_in_addr														:	STD_LOGIC;
	SIGNAL	en_counter_kernel,clr_counter_kernel,max_tick_kernel			:	STD_LOGIC;
	SIGNAL	clr_counter_channels,max_tick_channels								:	STD_LOGIC;
	SIGNAL	en_counter_filters,clr_counter_filters,max_tick_filters		:	STD_LOGIC;
	
	SIGNAL	enable_data_index,padding_flag										:	STD_LOGIC;
--	SIGNAL	data_ready_conv2D,
	SIGNAL	data_ready_kernel															:	STD_LOGIC;
--	SIGNAL	en_counter_index,clr_counter_index,max_tick_index				:	STD_LOGIC;
	SIGNAL	enable_data_flatten_index,sel_data_in_addr						:	STD_LOGIC;
	SIGNAL	index_s,index_ch															:	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	index,index_s_std,index_ch_std										:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	SIGNAL	base_channel																:	UNSIGNED(MAX_COUNTER_WIDTH+BITS_SETUP_CONV2D+BITS_SETUP_CONV2D+1 DOWNTO 0);
--	SIGNAL	counter_index																:	STD_LOGIC_VECTOR(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
--	SIGNAL	max_counter_index															:	UNSIGNED(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);

	
	SIGNAL	sel_registers_input														:	STD_LOGIC_VECTOR(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
	SIGNAL	result_dot,result_acc													:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	result_dot_ch,bias_in,data_in_s										:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_in_flatten,data_in_flatten_s									:	STD_LOGIC_VECTOR(KERNEL_SIZE_2*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	kernel_in_flatten,kernel_in_flatten_s								:	STD_LOGIC_VECTOR(KERNEL_SIZE_2*BITS_FLOAT-1 DOWNTO 0);
	
	SIGNAL	max_counter_kernel,max_counter_channels,max_counter_filters	:	UNSIGNED(MAX_COUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	kernel_addr_u,bias_addr_u												:	UNSIGNED(MAX_COUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	kernel_addr,bias_addr													:	STD_LOGIC_VECTOR(MAX_COUNTER_WIDTH-1 DOWNTO 0);
--	SIGNAL															:	UNSIGNED(MAX_COUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	counter_kernel,counter_channel,counter_filter					:	UNSIGNED(MAX_COUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	counter_kernel_std														:	STD_LOGIC_VECTOR(MAX_COUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	counter_channel_s															:	UNSIGNED(BITS_SETUP_CONV2D+BITS_SETUP_CONV2D+MAX_COUNTER_WIDTH+1 DOWNTO 0);
	
----	SIGNAL	en_counter_out,clr_counter_out,max_tick_out						:	STD_LOGIC;
	SIGNAL	max_counter_out															:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1	DOWNTO 0);
BEGIN
	--========================================
	--                CIRCUIT               
	--========================================
	channels_out	<=	filters;
	--========================================
	--             FLAG CHANNELS
	--========================================
	flag_channels	<=	'0'	WHEN	to_integer(channels)=1	ELSE	'1';
	
	--========================================
	--              DATA FLATTEN
	--========================================
	sel_registers_input	<=	counter_kernel_std(SELECTOR_KERNEL_SIZE-1 DOWNTO 0);
	data_in_s	<=	ZEROS_DATA	WHEN	padding_flag='1'	ELSE
						data_in;
	data_flatten:	ENTITY	work.flatten_data
	GENERIC MAP	(	DATA_QUANTY		=>	KERNEL_SIZE_2,
						SIZE_SELECTOR	=>	SELECTOR_KERNEL_SIZE)
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						syn_clr			=>	syn_clr_s,
						sel				=>	sel_registers_input,
						enable			=>	enable_data_flatten,
						data_in			=> data_in_s,
						data_out			=>	data_in_flatten_s);

	--========================================
	--         REGISTER DATA FLATTEN
	--========================================
	reg_data_flatten: ENTITY  work.my_reg
	GENERIC MAP	(	DATA_WIDTH 	=> KERNEL_SIZE_2*BITS_FLOAT)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	enable_register_data_flatten,
						syn_clr		=>	syn_clr_s,
						d				=>	data_in_flatten_s,
						q				=> data_in_flatten);
	
	--========================================
	--             KERNEL FLATTEN
	--========================================
	kernel_flatten:	ENTITY	work.flatten_data
	GENERIC MAP	(	DATA_QUANTY		=>	KERNEL_SIZE_2,
						SIZE_SELECTOR	=>	SELECTOR_KERNEL_SIZE)
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						syn_clr			=>	syn_clr_s,
						sel				=>	sel_registers_input,
						enable			=>	enable_data_flatten,
						data_in			=> kernel_in,
						data_out			=>	kernel_in_flatten_s);
						
	--========================================
	--        REGISTER KERNEL FLATTEN
	--========================================
	reg_kernel_flatten: ENTITY  work.my_reg
	GENERIC MAP	(	DATA_WIDTH 	=> KERNEL_SIZE_2*BITS_FLOAT)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	enable_register_data_flatten,
						syn_clr		=>	syn_clr_s,
						d				=>	kernel_in_flatten_s,
						q				=> kernel_in_flatten);
						
	--========================================
	--              DOT PRODUCT
	--========================================
	dot_product_unit: ENTITY work.dot_product
	PORT	MAP	(	rst				=>	rst,
						clk				=>	clk,
						syn_clr			=>	syn_clr_s,
						strobe			=>	strobe_dot_product,
						slice_mtx		=>	data_in_flatten,
						kernel			=>	kernel_in_flatten,
						result_dot		=>	result_dot,
						data_ready		=>	data_ready_dot,
						busy				=>	OPEN); -- port mapping
						
	--========================================
	--              ACCUMULATOR
	--========================================
	set_data_b_acc	<=	'0';
	strobe_acc		<=	data_ready_dot AND flag_channels;
	acc: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst,
						clk				=> clk,
						syn_clr			=> syn_clr_s,
						set_data_b		=> set_data_b_acc,
						strobe			=> strobe_acc,
						data_a			=>	result_dot,
						data_b			=> result_dot,
						result_acc		=> result_acc,
						data_ready		=> data_ready_acc,
						busy				=>	OPEN); -- port mapping
						
	--========================================
	--            MUX DATA CHANNEL
	--========================================
	mux_data_channel:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	result_dot, -- 0
						x2					=>	result_acc, -- 1
						sel				=>	flag_channels,
						y					=>	result_dot_ch); -- port mapping
						
	--========================================
	--             REGISTER BIAS
	--========================================
	reg_bias: ENTITY  work.my_reg
	GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	enable_register_bias,
						syn_clr		=>	syn_clr_s,
						d				=>	kernel_in,
						q				=> bias_in);
						
	--========================================
	--        ADDER SUBSTRACTOR FLOAT
	--========================================
	add_sub_s		<=	'1';
	strobe_add_sub	<=	(data_ready_dot AND NOT flag_channels) OR (data_ready_acc AND max_tick_channels);
	add_sub: ENTITY work.float_add_sub
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr_s,
							strobe		=> strobe_add_sub,
							add_sub		=>	add_sub_s,
							dataa			=> result_dot_ch,
							datab			=> bias_in,
							result		=> data_out,
							data_ready	=> data_ready_add_sub,
							busy			=>	OPEN); -- port mapping
						
	--========================================
	--          READ RAM KERNEL BIAS
	--========================================
	max_counter_kernel	<= to_unsigned(KERNEL_SIZE_2,MAX_COUNTER_WIDTH);
	max_counter_channels	<=	ZEROS_COUNTERS & channels;
	max_counter_filters	<=	ZEROS_COUNTERS & filters;
	------------- COUNTER KERNEL -------------
	counter_kernels:ENTITY WORK.counter
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						ena 				=>	en_counter_kernel,
						syn_clr			=>	syn_clr_s,
						clr_counter		=>	clr_counter_kernel,
						max_counter		=>	max_counter_kernel,
						counter_u		=>	counter_kernel,
						counter_std		=>	counter_kernel_std,
						max_tick			=>	max_tick_kernel);
	------------ COUNTER CHANNELS ------------
	counter_channels:ENTITY WORK.counter
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						ena 				=>	max_tick_kernel,
						syn_clr			=>	syn_clr_s,
						clr_counter		=>	clr_counter_channels,
						max_counter		=>	max_counter_channels,
						counter_u		=>	counter_channel,
						counter_std		=>	OPEN,
						max_tick			=>	max_tick_channels);
	------------ COUNTER FILTERS -------------
	counter_filters:ENTITY WORK.counter
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						ena 				=>	en_counter_filters,
						syn_clr			=>	syn_clr_s,
						clr_counter		=>	clr_counter_filters,
						max_counter		=>	max_counter_filters,
						counter_u		=>	counter_filter,
						counter_std		=>	OPEN,
						max_tick			=>	max_tick_filters);
	----------- INDEX KERNEL/BIAS ------------
	counter_channel_s	<=	counter_channel*rows*columns;
	kernel_addr_u		<=	counter_kernel+counter_channel_s(ADDR_KERNEL_WIDTH-1 DOWNTO 0)+kernel_base;
	bias_addr_u			<=	counter_filter+bias_base;
	kernel_addr			<=	STD_LOGIC_VECTOR(kernel_addr_u);
	bias_addr			<=	STD_LOGIC_VECTOR(bias_addr_u);
	---------- MUX KERNEL IN ADDR ------------
	mux_kernel_in_addr:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	ADDR_KERNEL_WIDTH) -- generic mapping
	PORT	MAP	(	x1					=>	kernel_addr, -- 0
						x2					=>	bias_addr, -- 1
						sel				=>	sel_kernel_in_addr,
						y					=>	kernel_in_rdaddress); -- port mapping

	--========================================
	--          READ RAM KERNEL BIAS
	--========================================
	-------------- DATA CONV2D ---------------
	data_index: ENTITY work.data_conv2D
	PORT	MAP	(	rst					=>	rst,
						clk					=>	clk,
						syn_clr				=>	syn_clr_s,
						enable				=>	enable_data_index,
						padding_sel			=>	padding,
						columns_in			=>	columns,
						rows_in				=>	rows,
						data_ready			=>	OPEN,
						data_ready_kernel	=>	data_ready_kernel,
						padding				=>	padding_flag,
						index_out			=>	index,
						columns_out			=> columns_out,
						rows_out				=>	rows_out); -- port mapping
--	------------- COUNTER INDEX --------------
--	max_counter_index	<= to_unsigned(KERNEL_SIZE_2,SELECTOR_KERNEL_SIZE);
--	counter_index_unit:ENTITY WORK.counter
--	GENERIC MAP	(	MAXCOUNTER_WIDTH	=>	SELECTOR_KERNEL_SIZE)
--	PORT MAP		(	clk					=>	clk,
--						rst					=>	rst,
--						ena 					=>	en_counter_index,
--						syn_clr				=>	syn_clr_s,
--						clr_counter			=>	clr_counter_index,
--						max_counter			=>	max_counter_index,
--						counter_u			=>	OPEN,
--						counter_std			=>	counter_index,
--						max_tick				=>	max_tick_index);
	----------- INDEX FLATTEN MUX ------------
	enable_data_flatten_index	<=	enable_data_flatten AND flag_channels;
	index_flatten_mux:	ENTITY	work.flatten_data_mux
	GENERIC MAP	(	DATA_WIDTH		=>	ADDR_BUFFERS_WIDTH,
						DATA_QUANTY		=>	KERNEL_SIZE_2,
						SIZE_SELECTOR	=>	SELECTOR_KERNEL_SIZE)
	PORT MAP		(	clk				=>	clk,
						rst				=>	rst,
						syn_clr			=>	syn_clr_s,
						sel				=>	sel_registers_input,
						enable			=>	enable_data_flatten_index,
						data_in			=> index,
						data_out			=>	index_s_std);
	------------------------------------------
	index_s			<=	UNSIGNED(index_s_std);
	base_channel	<=	rows*columns*counter_channel;
	index_ch			<=	index_s+base_channel(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	----------- MUX DATA IN ADDR -------------
	index_ch_std	<=	STD_LOGIC_VECTOR(index_ch);
	mux_data_in_addr:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	ADDR_BUFFERS_WIDTH) -- generic mapping
	PORT	MAP	(	x1					=>	index_s_std, -- 0
						x2					=>	index_ch_std, -- 1
						sel				=>	sel_data_in_addr,
						y					=>	data_in_rdaddress); -- port mapping

	--========================================
	--            WRITE RAM OUTPUT
	--========================================
	data_out_wren		<=	data_ready_add_sub;
	max_counter_out	<=	(OTHERS	=>	'1');
	------------- COUNTER OUTPUT -------------
	counter_out:ENTITY WORK.counter
	GENERIC MAP	(	MAXCOUNTER_WIDTH	=>	ADDR_BUFFER_INTERNAL_WIDTH)
	PORT MAP		(	clk					=>	clk,
						rst					=>	rst,
						ena 					=>	data_ready_add_sub,
						syn_clr				=>	syn_clr_s,
						clr_counter			=>	syn_clr_s,
						max_counter			=> max_counter_out,
						counter_u			=>	OPEN,
						counter_std			=>	data_out_wraddress,
						max_tick				=>	OPEN);
	
	--========================================
	--                    FSM
	--========================================
	------------ Sequential Section -------------
	seq_fsm: PROCESS(clk,rst)
	BEGIN
		IF (rst = '1') THEN
			pr_state 				<=	idle;
		ELSIF(rising_edge(clk)) THEN
			IF	(syn_clr='1')	THEN
				pr_state 				<= idle;
			ELSE
				pr_state 				<= nx_state;
			END IF;
		END IF;
	END PROCESS;
	----------- Combinational Section -----------
	comb_fsm: PROCESS (pr_state,strobe,data_ready_kernel,max_tick_filters,max_tick_channels,data_ready_dot,data_ready_add_sub,max_tick_kernel,flag_channels)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				syn_clr_s								<=	'1';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'1';
				clr_counter_channels					<=	'1';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'1';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				IF (strobe = '1') THEN
					nx_state	<= take_bias;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN f_take_bias =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'1';
				sel_kernel_in_addr					<=	'1';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'1';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				nx_state									<= take_data_kernel;
			---------------------------
			WHEN f_take_data_kernel =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'1';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'1';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'1';
				sel_data_in_addr						<=	'0';
				IF (data_ready_kernel = '1') THEN
					nx_state	<= data_kernel;
				ELSE
					nx_state	<= take_data_kernel;
				END IF;
			---------------------------
			WHEN f_data_kernel =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'1';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				nx_state									<= start_dot;
			---------------------------
			WHEN start_dot =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'1';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				IF (flag_channels='1' AND max_tick_channels='0') THEN
					nx_state	<= take_channel;
				ELSIF (flag_channels='0' OR max_tick_channels='1') THEN
					nx_state	<= take_bias;
				ELSE
					nx_state	<= start_dot;
				END IF;
			---------------------------
			WHEN take_channel =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'1';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'1';
				IF (max_tick_kernel='1') THEN
					nx_state	<= dot;
				ELSE
					nx_state	<= take_channel;
				END IF;
			---------------------------
			WHEN take_bias =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'1';
				sel_kernel_in_addr					<=	'1';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'1';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				nx_state									<= take_data_kernel;
			---------------------------
			WHEN take_data_kernel =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'1';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'1';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'1';
				sel_data_in_addr						<=	'0';
				IF (data_ready_kernel = '1') THEN
					nx_state	<= data_kernel;
				ELSE
					nx_state	<= take_data_kernel;
				END IF;
			---------------------------
			WHEN data_kernel =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'1';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				nx_state									<= dot;
			---------------------------
			WHEN dot	=>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'1';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				IF (max_tick_filters='1' AND max_tick_channels='1' AND data_ready_dot='1') THEN
					nx_state	<= wait_finish;
				ELSIF (data_ready_dot='1') THEN
					nx_state	<= start_dot;
				ELSE
					nx_state	<= dot;
				END IF;
			---------------------------
			WHEN wait_finish =>
				syn_clr_s								<=	'0';
				data_ready								<=	'0';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				IF (data_ready_add_sub='1') THEN
					nx_state	<= ready;
				ELSE
					nx_state	<= wait_finish;
				END IF;
			---------------------------
			WHEN ready =>
				syn_clr_s								<=	'0';
				data_ready								<=	'1';
				enable_data_flatten					<=	'0';
				enable_register_data_flatten		<=	'0';
				strobe_dot_product					<=	'0';
				enable_register_bias					<=	'0';
				sel_kernel_in_addr					<=	'0';
				en_counter_kernel						<=	'0';
				clr_counter_kernel					<=	'0';
				clr_counter_channels					<=	'0';
				en_counter_filters					<=	'0';
				clr_counter_filters					<=	'0';
				enable_data_index						<=	'0';
				sel_data_in_addr						<=	'0';
				nx_state									<= idle;
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE arch_conv2D; -- Architecture