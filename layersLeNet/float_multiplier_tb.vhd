LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK;
USE WORK.package_lenet.all;
--------------------------------------------------------
ENTITY float_multiplier_tb IS 
	GENERIC	(	DATA_WIDTH		:	INTEGER	:=	32;
					LATENCY			:	UNSIGNED(LATENCY_WIDTH-1 DOWNTO 0)	:=	FLOAT_MULT_LATENCY);	
END ENTITY float_multiplier_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_float_multiplier OF float_multiplier_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================		
	SIGNAL	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			: 	STD_LOGIC := '1';
	SIGNAL	syn_clr_tb		:	STD_LOGIC;
	SIGNAL	strobe_tb		: 	STD_LOGIC;
	SIGNAL	dataa_tb			: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	datab_tb			: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	result_tb		: 	STD_LOGIC_VECTOR (DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_ready_tb	:	STD_LOGIC;
	SIGNAL	busy_tb			:	STD_LOGIC;	
BEGIN
	--========================================
	--                RUN 140ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb 		<= '0' AFTER 20ns; -- reset is modeled
	clk_tb 		<= NOT clk_tb AFTER 10ns; -- time is modeled
	syn_clr_tb	<= '0';
	strobe_tb	<=	'0','1' AFTER 40ns,'0' AFTER 60ns;
	dataa_tb		<=	"01000000000000000000000000000000";
	datab_tb		<=	"01000000010000000000000000000000";
	
	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.float_multiplier
	GENERIC	MAP	(	DATA_WIDTH	=>	DATA_WIDTH,
							LATENCY		=>	LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst_tb,
							clk			=> clk_tb,
							syn_clr		=> syn_clr_tb,
							strobe		=> strobe_tb,
							dataa			=> dataa_tb,
							datab			=> datab_tb,
							result		=> result_tb,
							data_ready	=> data_ready_tb,
							busy			=> busy_tb); -- port mapping	
END ARCHITECTURE testbench_float_multiplier;