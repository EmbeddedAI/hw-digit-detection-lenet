onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/COLUMNS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/ROWS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/KERNEL_SIZE
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/STRIDE
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/rst
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/clk
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/syn_clr
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/strobe
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/kernel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/padding_sel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/feature_map
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/columns_out
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/rows_out
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_ready
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/pr_state
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/nx_state
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/columns_feauture_map
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/rows_feauture_map
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_padding
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_in
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_in_padding
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_kernel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_slice_kernel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/selector
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/selector_s
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/selector_next
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/ena_sel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/syn_clr_sel
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/flag_ready
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/strobe_dot_product
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/syn_clr_dot_product
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/data_ready_dot_product
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/result_dot
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/NEW_ROWS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/NEW_COLUMNS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/NEW_COLUMNS_PADDING
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/NEW_ROWS_PADDING
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/PADDING
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/PADDING_COLUMNS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/PADDING_ROWS
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/SIZE_SELECTOR
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/ZEROS_DATA_IN
add wave -noupdate /conv2d_channel_test_tb/DUT/TEST_B/SELECTOR_ZEROS
add wave -noupdate /conv2d_channel_test_tb/enable_tb
add wave -noupdate /conv2d_channel_test_tb/check_tb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19497 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 508
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
run 854600ns
WaveRestoreZoom {0 ns} {854600ns ns}
