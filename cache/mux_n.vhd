LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--------------------------------------------
ENTITY	mux_n	IS
	GENERIC (N			:	INTEGER	:= 2;
				BIT_N		:	INTEGER	:= 4);
	PORT(		data_in	:	IN		STD_LOGIC_VECTOR(((2**N)*BIT_N)-1 DOWNTO 0);
				sel		:	IN		STD_LOGIC_VECTOR(N-1 DOWNTO 0);
				data_out	:	OUT	STD_LOGIC_VECTOR(BIT_N-1 DOWNTO 0));
END ENTITY mux_n;
--------------------------------------------
ARCHITECTURE mux_n_arch	OF mux_n	IS
	SIGNAL	sel_int	:	INTEGER	:=	to_integer(unsigned(sel));
BEGIN
	--circuit
	data_out	<=	data_in(BIT_N*(sel_int+1)-1 DOWNTO BIT_N*sel_int);
END ARCHITECTURE mux_n_arch;