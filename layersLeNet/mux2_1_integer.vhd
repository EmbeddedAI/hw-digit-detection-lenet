LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY mux2_1_integer IS 
	PORT	 (	x1				:	IN		INTEGER;
				x2				:	IN		INTEGER;
				sel			:	IN		STD_LOGIC;
				y				:	OUT	INTEGER); -- Define I/O Ports
END ENTITY mux2_1_integer; -- Entity 
--------------------------------------------------------
ARCHITECTURE functional OF mux2_1_integer is 
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	y	<=	x2	WHEN	sel = '1'	ELSE
			x1;
END ARCHITECTURE functional; -- Architecture