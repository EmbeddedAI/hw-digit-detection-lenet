------------------------------------------------------------------------
--						ram_dp
-- Date: 2022-07-03
-- Version: 1.0 
-- Description:
-- 
-- Notes:
-- 	To be inferred as RAM memory element the minimum ADDR_WIDTH is 3 
--		for a total of 8 elements in memory.
------------------------------------------------------------------------
LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE ieee.numeric_std.all; 
--------------------------------------------------------
ENTITY ram_dp IS 
	GENERIC (DATA_WIDTH	: INTEGER	:= 32;  
				ADDR_WIDTH	: INTEGER	:=	5);
	PORT(    clock	      : IN 	STD_LOGIC;
				data	 	   : IN 	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
				rdaddress 	: IN 	STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
				wraddress	: IN 	STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0);
				wren	      : IN 	STD_LOGIC;
				q	   		: OUT STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0)); 
END ENTITY ram_dp; 
ARCHITECTURE rtl OF ram_dp IS 
	--========================================
	--                 TYPES
	--========================================
	TYPE 		mem_2d_type IS ARRAY (0 TO (2**ADDR_WIDTH)-1) OF STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0); 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	ram:mem_2d_type; 
	SIGNAL	addr_reg	:	STD_LOGIC_VECTOR(ADDR_WIDTH-1 DOWNTO 0); 
BEGIN
	--========================================
	--                CIRCUIT               
	--========================================
	--========================================
	--             WRITE PROCESS
	--========================================
	write_process: PROCESS(clock)
		BEGIN 
			IF (rising_edge(clock)) THEN
				IF	(wren='1') THEN 
					ram(to_integer(unsigned(wraddress)))	<=	data; 
				END IF; 
				--========================================
				--              READ PROCESS
				--========================================
				q	<= ram(to_integer(unsigned(rdaddress)));
			END IF;
	END PROCESS; 
	 
END ARCHITECTURE rtl;