LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_product IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	KERNEL_SIZE;
					SIZE_SELECTOR	:	INTEGER	:=	SELECTOR_KERNEL_SIZE); -- Define Generic Parameters
	PORT		(	rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					strobe			:	IN 	STD_LOGIC;
					slice_mtx		:	IN		STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
					kernel			:	IN		STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
					result_dot		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_ready		:	OUT 	STD_LOGIC;
					busy				:	OUT 	STD_LOGIC); -- Define I/O Ports
END ENTITY dot_product; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_dot_product OF dot_product is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	SELECTOR_ZEROS							:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0)	:=	(OTHERS	=>	'0');
	CONSTANT	SELECTOR_ZEROS_ONE					:	UNSIGNED(SIZE_SELECTOR-2 DOWNTO 0)	:=	(OTHERS	=>	'0');
	--========================================
	--                 TYPES
	--========================================
	TYPE state IS (idle,charge_data,wait_charge,operating_dot_row,charge_acc_first,wait_acc_first,operating_acc_first,charge_acc,wait_acc,operating_acc,ready);
	--========================================
	--                 SIGNALS                
	--========================================	
	SIGNAL 	pr_state,nx_state						: 	state;
	SIGNAL	strobe_dot_row,flag_ready			:	STD_LOGIC;
	SIGNAL	ena_sel,sync_clr_sel_one			:	STD_LOGIC;
	SIGNAL	data_mtx									:	STD_LOGIC_VECTOR(KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_kernel								:	STD_LOGIC_VECTOR(KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	accumulate_rows						:	STD_LOGIC_VECTOR(KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	selector									:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL 	selector_s,selector_next			:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	data_ready_dot_rows_s				:	STD_LOGIC_VECTOR(KERNEL_SIZE-1 DOWNTO 0);
	SIGNAL	data_ready_dot_rows					:	STD_LOGIC;
	SIGNAL	set_data_b_acc							:	STD_LOGIC;
	SIGNAL	data_a_acc,result_acc				:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	strobe_acc,data_ready_acc			:	STD_LOGIC;
	SIGNAL	syn_clr_s,sync_clr_sel				:	STD_LOGIC;
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	--========================================
	--           ROWS SEPARATE DATA
	--========================================
	separate_data: ENTITY work.rows_separate
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	data_in			=>	slice_mtx,
						selector			=>	selector,
						data_out			=>	data_mtx); -- port mapping

	--========================================
	--          ROWS SEPARATE KERNEL
	--========================================
	separate_kernel: ENTITY work.rows_separate
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	data_in			=>	kernel,
						selector			=>	selector,
						data_out			=>	data_kernel); -- port mapping

	--========================================
	--            DOT PRODUCT ROWS
	--========================================
	dot_rows:	FOR i IN  0	TO	KERNEL_SIZE-1	GENERATE
		dot_row_i: ENTITY work.dot_rows
		GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
							SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
		PORT	MAP	(	rst				=>	rst,
							clk				=>	clk,
							syn_clr			=>	syn_clr_s,
							strobe			=>	strobe_dot_row,
							data_a			=>	data_mtx(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							data_b			=>	data_kernel(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							result_dot		=>	accumulate_rows(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							data_ready		=>	data_ready_dot_rows_s(i),
							busy				=>	OPEN); -- port mapping
	END GENERATE;
	
	--========================================
	--      OR REDUCTION DATA READY ROWS
	--========================================
	data_ready_r: ENTITY work.or_reduction
	GENERIC	MAP(	MAX_WIDTH	=>	KERNEL_SIZE) -- generic mapping
	PORT	MAP	(	in_vector	=>	data_ready_dot_rows_s,
						q				=>	data_ready_dot_rows); -- port mapping

	--========================================
	--         MUX DATA A ACCUMULATOR
	--========================================
	mux_data_a: ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=>	KERNEL_SIZE*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	accumulate_rows,
						sel				=>	selector,
						data_out			=>	data_a_acc); -- port mapping

	--========================================
	--              ACCUMULATOR
	--========================================
	acc: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst,
						clk				=> clk,
						syn_clr			=> syn_clr_s,
						set_data_b		=> set_data_b_acc,
						strobe			=> strobe_acc,
						data_a			=>	data_a_acc,
						data_b			=> accumulate_rows(BITS_FLOAT-1 DOWNTO 0),
						result_acc		=> result_acc,
						data_ready		=> data_ready_acc,
						busy				=>	OPEN); -- port mapping
						
	--========================================
	--              SUM SELECTOR
	--========================================
	selector_next	<=	SELECTOR_ZEROS					WHEN	sync_clr_sel='1'		ELSE
							SELECTOR_ZEROS_ONE & '1'	WHEN	sync_clr_sel_one='1'	ELSE
							selector_s+1					WHEN	ena_sel='1'				ELSE
							selector_s;

	--========================================
	--               FLAG READY
	--========================================
	flag_ready		<=	'1'	WHEN selector_s=to_unsigned(KERNEL_SIZE,SIZE_SELECTOR)	ELSE	'0';

	--===========================================
	--                    FSM
	--===========================================
	------------ Sequential Section -------------
	seq_fsm: PROCESS(clk,rst)
		VARIABLE temp_selector	:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			pr_state 		<=	idle;
			temp_selector	:=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			IF(syn_clr='1')		THEN
				pr_state 		<= idle;
				temp_selector	:=	(OTHERS=>'0');
			ELSE
				pr_state 		<= nx_state;
				temp_selector	:=	selector_next;
			END IF;
		END IF;
		selector		<=	STD_LOGIC_VECTOR(temp_selector);
		selector_s	<=	temp_selector;
	END PROCESS;
	----------- Combinational Section -----------
	comb_fsm: PROCESS (pr_state,strobe,data_ready_dot_rows,data_ready_acc,flag_ready,result_acc)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				busy						<=	'0';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'1';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'1';
				IF (strobe = '1') THEN
					nx_state	<= charge_data;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN charge_data =>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'1';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				nx_state					<= wait_charge; 
			---------------------------
			WHEN wait_charge =>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'1';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				nx_state					<= operating_dot_row; 
			---------------------------
			WHEN operating_dot_row =>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				IF (data_ready_dot_rows = '1' AND flag_ready = '1') THEN
					nx_state	<= charge_acc_first;
				ELSIF (data_ready_dot_rows = '1' AND flag_ready = '0') THEN
					nx_state	<=	charge_data;
				ELSE
					nx_state	<= operating_dot_row;
				END IF;
			---------------------------
			WHEN charge_acc_first	=>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'1';
				set_data_b_acc			<=	'1';
				syn_clr_s				<=	'0';
				nx_state					<= wait_acc_first;
			---------------------------
			WHEN wait_acc_first =>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'1';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '1';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'1';
				syn_clr_s				<=	'0';
				nx_state					<= operating_acc_first; 
			---------------------------
			WHEN operating_acc_first	=>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'1';
				syn_clr_s				<=	'0';
				IF (data_ready_acc = '1') THEN
					nx_state	<= charge_acc;
				ELSE
					nx_state	<= operating_acc_first;
				END IF;
			---------------------------
			WHEN charge_acc	=>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '1';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				nx_state					<= wait_acc;
			---------------------------
			WHEN wait_acc	=>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'1';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				nx_state					<= operating_acc; 
			---------------------------
			WHEN operating_acc 		=>
				busy						<=	'1';
				data_ready				<= '0';
				ena_sel					<=	'0';
				result_dot				<=	(OTHERS=>'0');
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'0';
				IF (data_ready_acc = '1' AND flag_ready = '1') THEN
					nx_state	<= ready;
				ELSIF (data_ready_acc = '1' AND flag_ready = '0') THEN
					nx_state	<=	charge_acc;
				ELSE
					nx_state	<= operating_acc;
				END IF;
			---------------------------
			WHEN ready =>
				busy						<=	'1';
				data_ready				<= '1';
				ena_sel					<=	'0';
				result_dot				<=	result_acc;
				strobe_dot_row			<=	'0';
				strobe_acc				<= '0';
				sync_clr_sel			<=	'0';
				sync_clr_sel_one		<=	'0';
				set_data_b_acc			<=	'0';
				syn_clr_s				<=	'1';
				nx_state					<= idle; 
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE arch_dot_product; -- Architecture