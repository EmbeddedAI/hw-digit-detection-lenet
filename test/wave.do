onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /float_multiplier_tb/DUT/DATA_WIDTH
add wave -noupdate /float_multiplier_tb/DUT/LATENCY
add wave -noupdate /float_multiplier_tb/DUT/rst
add wave -noupdate /float_multiplier_tb/DUT/clk
add wave -noupdate /float_multiplier_tb/DUT/syn_clr
add wave -noupdate /float_multiplier_tb/DUT/strobe
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/dataa
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/datab
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/result
add wave -noupdate /float_multiplier_tb/DUT/data_ready
add wave -noupdate /float_multiplier_tb/DUT/busy
add wave -noupdate /float_multiplier_tb/DUT/pr_state
add wave -noupdate /float_multiplier_tb/DUT/nx_state
add wave -noupdate /float_multiplier_tb/DUT/en_counter_s
add wave -noupdate /float_multiplier_tb/DUT/clr_counter_s
add wave -noupdate /float_multiplier_tb/DUT/max_tick
add wave -noupdate /float_multiplier_tb/DUT/busy_s
add wave -noupdate /float_multiplier_tb/DUT/ena_mult
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/dataa_reg
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/datab_reg
add wave -noupdate -radix float32 /float_multiplier_tb/DUT/result_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 473
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {140 ns}
run 140ns
