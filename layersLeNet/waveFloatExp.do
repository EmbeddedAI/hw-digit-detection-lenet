onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /float_exp_tb/DATA_WIDTH
add wave -noupdate /float_exp_tb/LATENCY
add wave -noupdate /float_exp_tb/rst_tb
add wave -noupdate /float_exp_tb/clk_tb
add wave -noupdate /float_exp_tb/syn_clr_tb
add wave -noupdate /float_exp_tb/strobe_tb
add wave -noupdate -radix float32 /float_exp_tb/data_tb
add wave -noupdate -radix float32 /float_exp_tb/result_tb
add wave -noupdate /float_exp_tb/data_ready_tb
add wave -noupdate /float_exp_tb/busy_tb
add wave -noupdate /float_exp_tb/DUT/altfp_div/aclr
add wave -noupdate /float_exp_tb/DUT/altfp_div/clk_en
add wave -noupdate /float_exp_tb/DUT/altfp_div/clock
add wave -noupdate -radix float32 /float_exp_tb/DUT/altfp_div/data
add wave -noupdate -radix float32 /float_exp_tb/DUT/altfp_div/result
add wave -noupdate -radix float32 /float_exp_tb/DUT/altfp_div/sub_wire0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {379549 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 502
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
run 400ns
WaveRestoreZoom {20950 ps} {419950 ps}
