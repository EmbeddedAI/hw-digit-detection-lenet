onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /test_dot_product_tb/DUT/KERNEL_SIZE
add wave -noupdate /test_dot_product_tb/DUT/SIZE_SELECTOR
add wave -noupdate /test_dot_product_tb/DUT/rst
add wave -noupdate /test_dot_product_tb/DUT/clk
add wave -noupdate /test_dot_product_tb/DUT/syn_clr
add wave -noupdate /test_dot_product_tb/DUT/strobe
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/result_dot
add wave -noupdate /test_dot_product_tb/DUT/data_ready
add wave -noupdate /test_dot_product_tb/DUT/busy
add wave -noupdate /test_dot_product_tb/DUT/slice_mtx_s
add wave -noupdate /test_dot_product_tb/DUT/kernel_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/DATA_WIDTH
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/LATENCY
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/rst
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/clk
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/syn_clr
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/strobe
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/dataa
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/datab
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/result
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/data_ready
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/busy
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/pr_state
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/nx_state
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/en_counter_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/clr_counter_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/max_tick
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/busy_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/multiplier/ena_mult
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/dataa_reg
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/datab_reg
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/multiplier/result_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/DATA_WIDTH
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/LATENCY
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/rst
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/clk
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/syn_clr
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/strobe
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/add_sub
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/dataa
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/datab
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/result
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/data_ready
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/busy
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/pr_state
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/nx_state
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/count_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/count_next
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/en_counter_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/clr_counter_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/max_tick
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/busy_s
add wave -noupdate /test_dot_product_tb/DUT/dot_product/add_sub/ena_add_sub
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/dataa_reg
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/datab_reg
add wave -noupdate -radix float32 /test_dot_product_tb/DUT/dot_product/add_sub/result_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {81838 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 416
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {420 ns}
