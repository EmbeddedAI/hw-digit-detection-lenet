LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY demux_generic_test_tb IS 
	GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 5;
					DATA_WIDTH_IN	:	INTEGER	:=	32;
					DATA_WIDTH_OUT	:	INTEGER	:=	1024); -- Define Generic Parameters
END ENTITY demux_generic_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_demux_generic_test OF demux_generic_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data_in	:	STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
	SIGNAL	sel		:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	data_out	:	STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
--	SIGNAL	data_0,data_1,data_2,data_3 : STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
BEGIN

	data_in	<=	"01000011010111010000000000000000";
	sel		<=	"00011";
--	demux_generic_test_testbench_vectors : PROCESS                                                                        
--	BEGIN   
--		--========================================
--		--              TEST VECTORS
--		--========================================
--		---------------- vector 0 ----------------
--		sel	<=	"00";
--		WAIT FOR 20ns;
--		---------------- vector 1 ----------------
--		sel	<=	"01";
--		WAIT FOR 20ns;
--		---------------- vector 2 ----------------
--		sel	<=	"10";
--		WAIT FOR 20ns;
--		---------------- vector 3 ----------------
--		sel	<=	"11";
--		WAIT FOR 20ns;
--	WAIT;                                                       
--	END PROCESS demux_generic_test_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.demux_generic_test
	GENERIC MAP	(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=> DATA_WIDTH_IN,
						DATA_WIDTH_OUT	=>	DATA_WIDTH_OUT) -- Define Generic Parameters
	PORT MAP		(	data_in			=>	data_in,
						sel				=>	sel,
						data_out			=>	data_out);

	--========================================
	--                 OUTPUT
	--========================================	
--	data_0	<=	data_out(DATA_WIDTH_IN-1 DOWNTO 0);
--	data_1	<=	data_out(DATA_WIDTH_IN*2-1 DOWNTO DATA_WIDTH_IN);
--	data_2	<=	data_out(DATA_WIDTH_IN*3-1 DOWNTO DATA_WIDTH_IN*2);
--	data_3	<=	data_out(DATA_WIDTH_IN*4-1 DOWNTO DATA_WIDTH_IN*3);
END ARCHITECTURE testbench_demux_generic_test;