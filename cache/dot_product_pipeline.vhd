LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_product_pipeline IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
	PORT		(	rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					strobe			:	IN 	STD_LOGIC;
					slice_mtx		:	IN		STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
					kernel			:	IN		STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
					result_dot		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_ready		:	OUT 	STD_LOGIC;
					busy				:	OUT 	STD_LOGIC); -- Define I/O Ports
END ENTITY dot_product_pipeline; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_dot_product_pipeline OF dot_product_pipeline is 
	--SIGNALS
	TYPE state IS (idle,charge_data,wait_charge,operating,ready);
	SIGNAL 	pr_state,nx_state							: 	state;
	SIGNAL	strobe_mult,flag_ready					:	STD_LOGIC;
	SIGNAL	add_sub_s									:	STD_LOGIC;
	SIGNAL	ena_sel										:	STD_LOGIC;
	SIGNAL	data_mtx										:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_kernel									:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	selector										:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL 	selector_s,selector_next				:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	data_ready_mult,data_ready_add_sub	:	STD_LOGIC;
	SIGNAL	result_mult,result_add_sub				:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	sum_accumulate								:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
BEGIN 
	--circuit	
	add_sub_s	<=	'1';
	---------------Mux Data-------------------
	mux_data: ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=>	KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	slice_mtx,
						sel				=>	selector,
						data_out			=>	data_mtx); -- port mapping

	---------------Mux Kernel-------------------
	mux_kernel:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=>	KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	kernel,
						sel				=>	selector,
						data_out			=>	data_kernel); -- port mapping
						
	---------------Multipler-------------------
	multiplier: ENTITY work.float_multiplier
	GENERIC	MAP	(	DATA_WIDTH	=>	BITS_FLOAT,
							LATENCY		=>	FLOAT_MULT_LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr,
							strobe		=> strobe_mult,
							dataa			=> data_mtx,
							datab			=> data_kernel,
							result		=> result_mult,
							data_ready	=> data_ready_mult); -- port mapping

	---------------Add Sub-------------------
	add_sub: ENTITY work.float_add_sub
	GENERIC	MAP	(	DATA_WIDTH	=>	BITS_FLOAT,
							LATENCY		=>	FLOAT_ADD_LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr,
							strobe		=> data_ready_mult,
							add_sub		=>	add_sub_s,
							dataa			=> result_mult,
							datab			=> sum_accumulate,
							result		=> result_add_sub,
							data_ready	=> data_ready_add_sub); -- port mapping
							
	---------------Accumulate Register-------------------
	my_reg_add_sub: ENTITY  work.my_reg
	GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	data_ready_add_sub,
						syn_clr		=>	syn_clr,
						d				=>	result_add_sub,
						q				=> sum_accumulate);
		
	---------------Sum Selector-------------------
	selector_next	<=	selector_s+1	WHEN	ena_sel = '1'	ELSE
							selector_s;
	---------------Flag Ready-------------------
	flag_ready		<=	'1'	WHEN selector_s = to_unsigned(KERNEL_SIZE*KERNEL_SIZE,SIZE_SELECTOR) ELSE
							'0';
	--===========================================
	--              FSM
	--===========================================
	-- Sequential Section ----------------------
	seq_fsm: PROCESS(clk,rst)
		VARIABLE temp_selector	:	UNSIGNED(SIZE_SELECTOR-1 DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			pr_state 		<=	idle;
			temp_selector	:=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			pr_state 		<= nx_state;
			temp_selector	:=	selector_next;
		END IF;
		selector		<=	STD_LOGIC_VECTOR(temp_selector);
		selector_s	<=	temp_selector;
	END PROCESS;
	-- Combinational Section ----------------------
	comb_fsm: PROCESS (pr_state,strobe,data_ready_add_sub,flag_ready,sum_accumulate)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				busy			<=	'0';
				data_ready	<= '0';
				ena_sel		<=	'0';
				result_dot	<=	(OTHERS=>'0');
				strobe_mult	<=	'0';
				IF (strobe = '1') THEN
					nx_state	<= charge_data;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN charge_data =>
				busy			<=	'0';
				data_ready	<= '0';
				ena_sel		<=	'0';
				result_dot	<=	(OTHERS=>'0');
				strobe_mult	<=	'1';
				nx_state		<= wait_charge; 
			---------------------------
			WHEN wait_charge =>
				busy			<=	'0';
				data_ready	<= '0';
				ena_sel		<=	'1';
				result_dot	<=	(OTHERS=>'0');
				strobe_mult	<=	'0';
				nx_state		<= operating; 
			---------------------------
			WHEN operating =>
				busy			<=	'1';
				data_ready	<= '0';
				ena_sel		<=	'0';
				result_dot	<=	(OTHERS=>'0');
				strobe_mult	<=	'0';
				IF (data_ready_add_sub = '1' AND flag_ready = '1') THEN
					nx_state	<= ready;
				ELSIF (data_ready_add_sub = '1' AND flag_ready = '0') THEN
					nx_state	<=	charge_data;
				ELSE
					nx_state	<= operating;
				END IF;
			---------------------------
			WHEN ready =>
				busy			<=	'0';
				data_ready	<= '1';
				ena_sel		<=	'0';
				result_dot	<=	sum_accumulate;
				strobe_mult	<=	'0';
				nx_state		<= idle; 
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE arch_dot_product_pipeline; -- Architecture