LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY mtx_segmentation IS 
	GENERIC 	(	COLUMNS			:	INTEGER	:= 28;
					ROWS				:	INTEGER	:= 28;
					NEW_COLUMNS		:	INTEGER	:= 24;
					NEW_ROWS			:	INTEGER	:= 24;
					KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:=	10); -- Define Generic Parameters
	PORT	(		data_in		:	IN		STD_LOGIC_VECTOR(COLUMNS*ROWS*BITS_FLOAT-1 DOWNTO 0);
					data_out		:	OUT	STD_LOGIC_VECTOR(NEW_COLUMNS*NEW_ROWS*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY mtx_segmentation; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF mtx_segmentation is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL data		:	STD_LOGIC_VECTOR(NEW_COLUMNS*NEW_ROWS*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	data_in_rows_i: FOR i IN 0 TO NEW_ROWS-1 GENERATE
		data_in_columns_j: FOR j IN 0 TO NEW_COLUMNS-1 GENERATE
			data_in_kernel_k: FOR k_i IN 0 TO KERNEL_SIZE-1 GENERATE
					--========================================
					--              TAKE 25 DATA
					--========================================
				data_out(i*KERNEL_SIZE*KERNEL_SIZE*NEW_COLUMNS*BITS_FLOAT+j*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT+k_i*KERNEL_SIZE*BITS_FLOAT+KERNEL_SIZE*BITS_FLOAT-1 DOWNTO i*KERNEL_SIZE*KERNEL_SIZE*NEW_COLUMNS*BITS_FLOAT+j*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT+k_i*KERNEL_SIZE*BITS_FLOAT) <= data_in(i*COLUMNS*BITS_FLOAT+j*BITS_FLOAT+k_i*COLUMNS*BITS_FLOAT+KERNEL_SIZE*BITS_FLOAT-1 DOWNTO i*COLUMNS*BITS_FLOAT+j*BITS_FLOAT+k_i*COLUMNS*BITS_FLOAT);
			END GENERATE;
		END GENERATE;
	END GENERATE;
END ARCHITECTURE rtl; -- Architecture