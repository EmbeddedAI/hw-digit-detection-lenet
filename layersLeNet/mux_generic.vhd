LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE ieee.numeric_std.all;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY mux_generic IS 
	GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 2;
					DATA_WIDTH_IN	:	INTEGER	:=	128;
					DATA_WIDTH_OUT	:	INTEGER	:=	32); -- Define Generic Parameters
	PORT		(	data_in			:	IN		STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
					sel				:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1	DOWNTO 0);
					data_out			:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY mux_generic; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF mux_generic is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	ZEROS				:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)*DATA_WIDTH_OUT-DATA_WIDTH_IN-1 DOWNTO 0)	:=	(OTHERS => '0');
	--========================================
	--                 SIGNALS                
	--========================================
	TYPE		kernel			IS ARRAY(0 TO (2**SIZE_SELECTOR)-1) OF STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0);
	SIGNAL 	kernel_mtx		:	kernel;
	SIGNAL	data_s			:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)*DATA_WIDTH_OUT-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	data_s	<=	ZEROS & data_in;
	kernel_slice: FOR i IN 0 TO 2**SIZE_SELECTOR-1 GENERATE
		kernel_mtx(i)	<=	data_s(DATA_WIDTH_OUT*(i+1)-1 DOWNTO DATA_WIDTH_OUT*i);
	END GENERATE;

	--========================================
	--                 OUTPUT
	--========================================
	data_out	<= kernel_mtx(to_integer(unsigned(sel)));
END ARCHITECTURE rtl; -- Architecture