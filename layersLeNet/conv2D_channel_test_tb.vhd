LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY conv2D_channel_test_tb IS 
	GENERIC(	COLUMNS		:	INTEGER	:=	28;
				ROWS			:	INTEGER	:=	28;
				KERNEL_SIZE	:	INTEGER	:= 5;
				STRIDE		:	INTEGER	:=	1); -- Define Generic Parameters
END ENTITY conv2D_channel_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_conv2D_channel_test OF conv2D_channel_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			:	STD_LOGIC := '0';
	SIGNAL	syn_clr_tb		:	STD_LOGIC;
	SIGNAL	strobe_tb		:	STD_LOGIC;
	SIGNAL	padding_sel_tb	:	STD_LOGIC;
	SIGNAL	rows_out_tb		:	INTEGER;
	SIGNAL	columns_out_tb	:	INTEGER;
	SIGNAL	data_ready_tb	:	STD_LOGIC;
	SIGNAL	enable_tb		:	STD_LOGIC;
	SIGNAL	check_tb			:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 550ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb 			<= '0' AFTER 10ns; -- reset is modeled
	clk_tb 			<= NOT clk_tb AFTER 5ns; -- time is modeled
	syn_clr_tb		<=	'0';
	strobe_tb		<=	'0','1' AFTER 10ns,'0' AFTER 20ns;
	padding_sel_tb	<=	'1';
	enable_tb		<=	'1';
	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.conv2D_channel_test
	GENERIC	MAP(	COLUMNS		=>	COLUMNS,
						ROWS			=>	ROWS,
						KERNEL_SIZE	=>	KERNEL_SIZE,
						STRIDE		=>	STRIDE) -- generic mapping
	PORT	MAP	(	rst			=>	rst_tb,
						clk			=>	clk_tb,
						syn_clr		=> syn_clr_tb,
						strobe		=> strobe_tb,
						padding_sel	=> padding_sel_tb,
						rows_out		=> rows_out_tb,
						columns_out	=> columns_out_tb,
						data_ready	=> data_ready_tb,
						enable		=> enable_tb,
						check			=>	check_tb); -- port mapping

END ARCHITECTURE testbench_conv2D_channel_test;