LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
--LIBRARY WORK;
USE work.my_package.all;
LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;
--------------------------------------------------------
ENTITY float_matrix_mult IS 
	GENERIC	(	BLOCKS 		: natural := 0;
					CLUSTER 		: natural := 8;
					COLUMNSAA	: natural := 0;
					COLUMNSBB 	: natural := 0;
					ROWSAA		: natural := 0;
					VECTOR_SIZE : natural := 0;
					WIDTH_EXP 	: natural := 8;
					WIDTH_MAN 	: natural := 23); -- Define Generic Parameters
		PORT	(	rst			:	IN		STD_LOGIC;	
					sysclk		:	IN		STD_LOGIC;
					calcmatrix	:	IN		STD_LOGIC;
					enable		:	IN		STD_LOGIC;
					loadaa		:	IN		STD_LOGIC;
					loadbb		:	IN		STD_LOGIC;
					loaddata 	: 	IN 	STD_LOGIC_VECTOR(WIDTH_EXP+WIDTH_MAN+1-1 downto 0) := (others => '0');
					done			:	OUT	STD_LOGIC;
					outdata 		:	OUT 	STD_LOGIC_VECTOR(WIDTH_EXP+WIDTH_MAN+1-1 downto 0);
					outvalid		:	OUT	STD_LOGIC;
					ready 		:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY float_matrix_mult; -- Entity 
--------------------------------------------------------
ARCHITECTURE fms_float_matrix_mult OF float_matrix_mult is 
	--SIGNALS
	component altfp_matrix_mult
--	generic (
--	blocks : natural := 0;
--	cluster : natural := 8;
--	columnsaa : natural := 0;
--	columnsbb : natural := 0;
--	intended_device_family : string := "unused";
--	rowsaa : natural := 0;
--	vectorsize : natural := 0;
--	width_exp : natural := 8;
--	width_man : natural := 23;
--	lpm_hint : string := "UNUSED";
--	lpm_type : string := "altfp_matrix_mult"
--	);
	port(
	calcmatrix : in std_logic := '0';
	done : out std_logic;
	enable : in std_logic := '1';
	loadaa : in std_logic := '0';
	loadbb : in std_logic := '0';
	loaddata : in std_logic_vector(width_exp+width_man+1-1 downto 0) := (others => '0');
	outdata : out std_logic_vector(width_exp+width_man+1-1 downto 0);
	outvalid : out std_logic;
	ready : out std_logic;
	reset : in std_logic := '0';
	sysclk : in std_logic
	);
	end component;
BEGIN 
	--circuit
	fmm: COMPONENT altfp_matrix_mult
--	GENERIC MAP	(	BLOCKS 		=>	BLOCKS,
--						CLUSTER 		=> CLUSTER,
--						COLUMNSAA	=> COLUMNSAA,
--						COLUMNSBB 	=> COLUMNSBB,
--						ROWSAA		=> ROWSAA,
--						VECTORSIZE 	=> VECTOR_SIZE) -- generic mapping
	PORT MAP		(	reset			=>	rst,
						sysclk		=> sysclk,
						calcmatrix	=>	calcmatrix,
						enable		=>	enable,
						loadaa		=>	loadaa,
						loadbb		=>	loadbb,
						loaddata		=>	loaddata,
						done			=>	done,
						outdata		=>	outdata,
						outvalid		=>	outvalid,
						ready			=> ready); -- port mapping
END ARCHITECTURE fms_float_matrix_mult; -- Architecture