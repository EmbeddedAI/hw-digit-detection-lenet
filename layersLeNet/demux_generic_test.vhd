LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--------------------------------------------------------------
ENTITY demux_generic_test IS 
	GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 5;
					DATA_WIDTH_IN	:	INTEGER	:=	32;
					DATA_WIDTH_OUT	:	INTEGER	:=	1024); -- Define Generic Parameters
	PORT		(	data_in	:	IN		STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
					sel		:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					data_out	:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0));
END ENTITY demux_generic_test;
------------------------------------------------------------------
ARCHITECTURE test_demux_generi OF demux_generic_test	IS
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data_s		:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)*DATA_WIDTH_IN-1 DOWNTO 0);
	SIGNAL	data_demux	:	STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0);
BEGIN 
	
	--========================================
	--        TEST BLOCK INSTANTIATION        
	--=======================================
	TEST_B:	ENTITY work.demux_generic
	GENERIC MAP	(	SIZE_SELECTOR	=>	SIZE_SELECTOR,
						DATA_WIDTH_IN	=> DATA_WIDTH_IN,
						DATA_WIDTH_OUT	=>	DATA_WIDTH_OUT) -- Define Generic Parameters
	PORT MAP		(	data_in			=>	data_in,
						sel				=>	sel,
						data_out			=>	data_demux);

	--========================================
	--                 OUTPUT
	--========================================	
	data_out	<=	data_demux(DATA_WIDTH_IN*4-1 DOWNTO DATA_WIDTH_IN*3);
						
END ARCHITECTURE test_demux_generi;		