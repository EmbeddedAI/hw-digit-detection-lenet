LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY accumulator_tb IS 
END ENTITY accumulator_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_accumulator OF accumulator_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			:	STD_LOGIC := '1';
	SIGNAL	syn_clr_tb		:	STD_LOGIC;
	SIGNAL	set_data_b_tb	:	STD_LOGIC;
	SIGNAL	strobe_tb		:	STD_LOGIC;
	SIGNAL	data_a_tb		:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_b_tb		:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	result_acc_tb	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_ready_tb	:	STD_LOGIC;
	SIGNAL	busy_tb			:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 550ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb 		<= '0' AFTER 20ns; -- reset is modeled
	clk_tb 		<= NOT clk_tb AFTER 10ns; -- time is modeled
	syn_clr_tb	<=	'0';
	strobe_tb	<=	'0','1' AFTER 40ns,'0' AFTER 60ns,'1' AFTER 210ns,'0' AFTER 230ns,'1' AFTER 380ns,'0' AFTER 400ns;
	
	accumulator_testbench_vectors : PROCESS                                                                                  
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		set_data_b_tb	<=	'1';
		data_a_tb		<=	"00111111000011001100110011001101";
		data_B_tb		<=	"00111111110000000000000000000000";
		WAIT FOR 210ns;
		---------------- vector 1 ----------------
		set_data_b_tb	<=	'0';
		data_a_tb		<=	"01000000000110011001100110011010";
		data_B_tb		<=	"00000000000000000000000000000000";
		WAIT FOR 170ns;
		---------------- vector 2 ----------------
		set_data_b_tb	<=	'0';
		data_a_tb		<=	"01000000000100110011001100110011";
		data_B_tb		<=	"00000000000000000000000000000000";
		WAIT FOR 170ns;
	WAIT;                                                       
	END PROCESS accumulator_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst_tb,
						clk				=> clk_tb,
						syn_clr			=> syn_clr_tb,
						set_data_b		=> set_data_b_tb,
						strobe			=> strobe_tb,
						data_a			=> data_a_tb,
						data_b			=> data_b_tb,
						result_acc		=> result_acc_tb,
						data_ready		=> data_ready_tb,
						busy				=>	busy_tb); -- port mapping

END ARCHITECTURE testbench_accumulator;