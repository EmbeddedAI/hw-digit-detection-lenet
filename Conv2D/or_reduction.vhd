LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY or_reduction IS 
	GENERIC	(	MAX_WIDTH	:	INTEGER	:=	5); -- Define Generic Parameters
	PORT		(	in_vector	:	IN		STD_LOGIC_VECTOR(MAX_WIDTH-1 DOWNTO 0);
					q				:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY or_reduction; -- Entity 
--------------------------------------------------------
ARCHITECTURE functional OF or_reduction is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	all_zeros	:	STD_LOGIC_VECTOR(MAX_WIDTH-1 DOWNTO 0)	:=	(OTHERS	=>	'0');
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	q	<=	'1'	WHEN	in_vector	>	all_zeros	ELSE	
			'0';
END ARCHITECTURE functional; -- Architecture