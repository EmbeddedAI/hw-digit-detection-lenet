LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE work.my_package.all;
--------------------------------------------------------
ENTITY conv2D IS 
	GENERIC(	COLUMNS			:	INTEGER	:=	10;
				ROWS				:	INTEGER	:=	10;
				KERNEL_SIZE		:	INTEGER	:= 3;
				STRIDE			:	INTEGER	:=	1;
				SIZE_SELECTOR	:	INTEGER	:=	6); -- Define Generic Parameters
	PORT	(	in_s	:	IN		STD_LOGIC_VECTOR(COLUMNS*ROWS-1	DOWNTO 0);
				out_s	:	OUT	STD_LOGIC_VECTOR(COLUMNS*ROWS-1	DOWNTO 0);); -- Define I/O Ports
END ENTITY conv2D; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_conv2D OF conv2D is
	--CONSTANTS
	CONSTANT	NEW_COLUMNS				:	INTEGER	:=	((COLUMNS-KERNEL_SIZE)/STRIDE)-1;
	CONSTANT	NEW_ROWS					:	INTEGER	:=	((ROWS-KERNEL_SIZE)/STRIDE)-1;
	CONSTANT	NEW_COLUMNS_PADDING	:	INTEGER	:=	COLUMNS;
	CONSTANT	NEW_ROWS_PADDING		:	INTEGER	:=	ROWS;
	CONSTANT	PADDING					:	INTEGER	:=	KERNEL_SIZE-1;
	CONSTANT	PADDING2					:	INTEGER	:=	PADDING/2;
	CONSTANT	PADDING_COLUMNS		:	INTEGER	:=	COLUMNS+PADDING;
	CONSTANT	PADDING_ROWS			:	INTEGER	:=	ROWS+PADDING;
	--SIGNALS
BEGIN 
	--circuit
	out_s	<=	in_s;
END ARCHITECTURE arch_conv2D; -- Architecture