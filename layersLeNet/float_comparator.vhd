LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY float_comparator IS 
	PORT	(	dataa	:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
				datab	:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
				lg		:	OUT	STD_LOGIC;
				eq		:	OUT	STD_LOGIC;
				ls		:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY float_comparator; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF float_comparator is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	EXPONENT_TOP					:	INTEGER										:=	30;
	CONSTANT	EXPONENT_BOTTOM				:	INTEGER										:=	23;
	CONSTANT	MANTISSA_TOP					:	INTEGER										:=	22;
	CONSTANT	MANTISSA_BOTTOM				:	INTEGER										:=	0;
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	lg_sign,lg_exponent,lg_mantissa	:	STD_LOGIC;
	SIGNAL	eq_sign,eq_exponent,eq_mantissa	:	STD_LOGIC;
	SIGNAL	ls_sign,ls_exponent,ls_mantissa	:	STD_LOGIC;
	SIGNAL	data_negative							:	STD_LOGIC;
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	--========================================
	--                  SIGN
	--========================================
	lg_sign			<=	'1' WHEN dataa(BITS_FLOAT-1)='0' AND datab(BITS_FLOAT-1)='1'	ELSE	'0';
	eq_sign			<=	'1' WHEN dataa(BITS_FLOAT-1)=datab(BITS_FLOAT-1)					ELSE	'0';
	ls_sign			<=	'1' WHEN dataa(BITS_FLOAT-1)='1' AND datab(BITS_FLOAT-1)='0'	ELSE	'0';
	data_negative	<= dataa(BITS_FLOAT-1) AND datab(BITS_FLOAT-1);
	
	--========================================
	--                EXPONENT
	--========================================
	lg_exponent		<=	'1' WHEN dataa(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)>datab(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM) AND data_negative='0'	ELSE	
							'1' WHEN dataa(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)<datab(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM) AND data_negative='1'	ELSE	
							'0';
	eq_exponent		<=	'1' WHEN dataa(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)=datab(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)								ELSE
							'0';
	ls_exponent		<=	'1' WHEN dataa(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)<datab(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM) AND data_negative='0'	ELSE
							'1' WHEN dataa(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM)>datab(EXPONENT_TOP DOWNTO EXPONENT_BOTTOM) AND data_negative='1'	ELSE	
							'0';
							
	--========================================
	--                MANTISSA
	--========================================
	lg_mantissa		<=	'1' WHEN dataa(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM)>datab(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM) AND data_negative='0'	ELSE
							'1' WHEN dataa(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM)<datab(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM) AND data_negative='1'	ELSE
							'0';
	eq_mantissa		<=	'1' WHEN dataa(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM)=datab(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM) 								ELSE
							'0';
	ls_mantissa		<=	'1' WHEN dataa(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM)<datab(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM) AND data_negative='0'	ELSE
							'1' WHEN dataa(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM)>datab(MANTISSA_TOP DOWNTO MANTISSA_BOTTOM) AND data_negative='1'	ELSE
							'0';
	
	--===========================================
	--                  OUTPUT
	--===========================================
	
	lg	<=	'1'	WHEN	lg_sign='1'															ELSE
			'1' 	WHEN  lg_exponent='1' AND eq_sign='1'								ELSE
			'1' 	WHEN	lg_mantissa='1' AND eq_sign='1' AND eq_exponent='1'	ELSE
			'0';
	
	eq	<=	eq_sign AND eq_exponent	AND eq_mantissa;
	
	ls	<=	'1'	WHEN	ls_sign='1'															ELSE
			'1' 	WHEN  ls_exponent='1' AND eq_sign='1'								ELSE
			'1' 	WHEN  ls_mantissa='1' AND eq_sign='1' AND eq_exponent='1'	ELSE
			'0';
END ARCHITECTURE rtl; -- Architecture