LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY softmax_tb IS 
END ENTITY softmax_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_softmax OF softmax_tb IS 
	--========================================
	--                CONSTANTS               
	--========================================
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	rst_tb						:	STD_LOGIC := '1';
	SIGNAL	clk_tb						:	STD_LOGIC := '0';
	SIGNAL	syn_clr_tb					:	STD_LOGIC;
	SIGNAL	strobe_tb					:	STD_LOGIC;
	SIGNAL	data_in_tb					:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	data_in_rdaddress_tb		:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	data_out_tb					:	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	data_out_wraddress_tb	:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
	SIGNAL	data_out_wren_tb			:	STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL	data_ready_tb				:	STD_LOGIC;
BEGIN
	--========================================
	--               RUN 2030ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb <= '0' AFTER 10ns; -- reset is modeled
	clk_tb <= NOT clk_tb AFTER 5ns; -- time is modeled
	syn_clr_tb	<=	'0';
	strobe_tb	<=	'0','1' AFTER 20ns,'0' AFTER 30ns;
	
	softmax_testbench_vectors : PROCESS                                               
		--========================================
		--         VARIABLE DECLARATIONS
		--========================================                                     
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		data_in_tb	<= "00111100101001111110111110011110";
		WAIT FOR 200ns;
		---------------- vector 1 ----------------
		data_in_tb	<= "00111011100011001110011100000100";
		WAIT FOR 170ns;
		---------------- vector 2 ----------------
		data_in_tb	<= "00111100001011000000100000110001";
		WAIT FOR 170ns;
		---------------- vector 3 ----------------
		data_in_tb	<= "00111101000001000100110100000001";
		WAIT FOR 170ns;
		---------------- vector 4 ----------------
		data_in_tb	<= "00111101000011010100111111011111";
		WAIT FOR 170ns;
		---------------- vector 5 ----------------
		data_in_tb	<= "00111010011010111110110111111010";
		WAIT FOR 170ns;
		---------------- vector 6 ----------------
		data_in_tb	<= "00111010101011111010001011110000";
		WAIT FOR 170ns;
		---------------- vector 7 ----------------
		data_in_tb	<= "00111111011111010010111100011011";
		WAIT FOR 170ns;
		---------------- vector 8 ----------------
		data_in_tb	<= "00111101001110000101000111101100";
		WAIT FOR 170ns;
		---------------- vector 9 ----------------
		data_in_tb	<= "00111101010111100110100110101101";
		WAIT FOR 170ns;
	WAIT;                                                       
	END PROCESS softmax_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.softmax
	PORT	MAP	(	rst						=> rst_tb,
						clk						=> clk_tb,
						syn_clr					=> syn_clr_tb,
						strobe					=> strobe_tb,
						data_in					=> data_in_tb,
						data_in_rdaddress		=> data_in_rdaddress_tb,
						data_out					=> data_out_tb,
						data_out_wraddress	=> data_out_wraddress_tb,
						data_out_wren			=> data_out_wren_tb(0),
						data_ready				=> data_ready_tb); -- port mapping
END ARCHITECTURE testbench_softmax;