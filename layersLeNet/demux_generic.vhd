LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--------------------------------------------------------------
ENTITY demux_generic IS 
	GENERIC	(	SIZE_SELECTOR	:	INTEGER	:= 2;
					DATA_WIDTH_IN	:	INTEGER	:=	32;
					DATA_WIDTH_OUT	:	INTEGER	:=	128); -- Define Generic Parameters
	PORT		(	data_in	:	IN		STD_LOGIC_VECTOR(DATA_WIDTH_IN-1 DOWNTO 0);
					sel		:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					data_out	:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH_OUT-1 DOWNTO 0));
END ENTITY demux_generic;
------------------------------------------------------------------
ARCHITECTURE arch_demux_generic OF demux_generic	IS
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data_s	:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)*DATA_WIDTH_IN-1 DOWNTO 0);
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	demux: FOR i IN 0 TO (2**SIZE_SELECTOR)-1 GENERATE	
		data_s(DATA_WIDTH_IN*(i+1)-1 DOWNTO DATA_WIDTH_IN*i)	<= data_in	WHEN sel = STD_LOGIC_VECTOR(to_unsigned(i,SIZE_SELECTOR)) ELSE
																					(OTHERS => '0');
	END GENERATE;

	--========================================
	--                 OUTPUT
	--========================================	
	data_out	<=	data_s(DATA_WIDTH_OUT-1 DOWNTO 0);
END ARCHITECTURE arch_demux_generic;		