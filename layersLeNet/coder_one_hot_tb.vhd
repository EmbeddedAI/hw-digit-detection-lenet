LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY coder_one_hot_tb IS 
	GENERIC(	N			:	INTEGER := 3);
END ENTITY coder_one_hot_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_coder_one_hot OF coder_one_hot_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	sel_tb	:	STD_LOGIC_VECTOR(N-1 DOWNTO 0);
	SIGNAL	f_tb		:	STD_LOGIC_VECTOR((2**N)-1 DOWNTO 0);
BEGIN
	coder_one_hot_testbench_vectors : PROCESS                                                                              
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		sel_tb	<=	"000";
		WAIT FOR 20ns;
		---------------- vector 1 ----------------
		sel_tb	<=	"001";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		sel_tb	<=	"010";
		WAIT FOR 20ns;
		---------------- vector 3 ----------------
		sel_tb	<=	"011";
		WAIT FOR 20ns;
		---------------- vector 4 ----------------
		sel_tb	<=	"100";
		WAIT FOR 20ns;
		---------------- vector 5 ----------------
		sel_tb	<=	"101";
		WAIT FOR 20ns;
		---------------- vector 6 ----------------
		sel_tb	<=	"110";
		WAIT FOR 20ns;
		---------------- vector 7 ----------------
		sel_tb	<=	"111";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS coder_one_hot_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	N) -- generic mapping
	PORT	MAP	(	sel	=>	sel_tb,
						f		=>	f_tb); -- port mapping

END ARCHITECTURE testbench_coder_one_hot;