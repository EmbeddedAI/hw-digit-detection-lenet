LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY accumulator IS 
	PORT		(	rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					set_data_b		:	IN 	STD_LOGIC;
					strobe			:	IN 	STD_LOGIC;
					data_a			:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_b			:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					result_acc		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_ready		:	OUT 	STD_LOGIC;
					busy				:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY accumulator; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_accumulator OF accumulator is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	ena_dff					:	STD_LOGIC;
	SIGNAL 	add_sub_s				:	STD_LOGIC;
	SIGNAL	data_ready_add_sub	:	STD_LOGIC;	
	SIGNAL 	data_b_s					:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	busy_add_sub			:	STD_LOGIC;
	SIGNAL 	result_add_sub			:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	sum_accumulate			:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	add_sub_s	<=	'1';
	ena_dff		<=	'1';

	--========================================
	--               MUX DATA B
	--========================================
	mux_i:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	x1					=>	sum_accumulate, -- 0
						x2					=>	data_b, -- 1
						sel				=>	set_data_b,
						y					=>	data_b_s); -- port mapping
						
	--========================================
	--        ADDER SUBSTRACTOR FLOAT
	--========================================
	add_sub: ENTITY work.float_add_sub
	GENERIC	MAP	(	DATA_WIDTH	=>	BITS_FLOAT,
							LATENCY		=>	FLOAT_ADD_LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr,
							strobe		=> strobe,
							add_sub		=>	add_sub_s,
							dataa			=> data_a,
							datab			=> data_b_s,
							result		=> result_add_sub,
							data_ready	=> data_ready_add_sub,
							busy			=>	busy_add_sub); -- port mapping
							
	--========================================
	--           ACCUMULATE REGISTER
	--========================================
	my_reg_add_sub: ENTITY  work.my_reg
	GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	data_ready_add_sub,
						syn_clr		=>	syn_clr,
						d				=>	result_add_sub,
						q				=> sum_accumulate);
						
	--========================================
	--           DATA READY OUTPUT
	--========================================
	dff_data_ready: ENTITY  work.my_dff
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						ena			=>	ena_dff,
						d				=>	data_ready_add_sub,
						q				=> data_ready);

	--========================================
	--                 OUTPUT
	--========================================
	result_acc	<=	sum_accumulate;
	busy			<=	'1' WHEN busy_add_sub = '1' OR data_ready_add_sub = '1' OR strobe = '1' ELSE	'0';
END ARCHITECTURE arch_accumulator; -- Architecture