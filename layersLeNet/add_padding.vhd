LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY add_padding IS 
	GENERIC(	COLUMNS				:	INTEGER	:=	28;
				ROWS					:	INTEGER	:=	28;
				PADDING				:	INTEGER	:=	4;
				PADDING_COLUMNS	:	INTEGER	:=	32;
				PADDING_ROWS		:	INTEGER	:=	32); -- Define Generic Parameters
	PORT	(	data_in				:	IN		STD_LOGIC_VECTOR(COLUMNS*ROWS*BITS_FLOAT-1 DOWNTO 0);
				data_out				:	OUT	STD_LOGIC_VECTOR(PADDING_COLUMNS*PADDING_ROWS*BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY add_padding; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF add_padding is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	PADDING2						:	INTEGER																	:=	PADDING/2;
	CONSTANT	ROW_ZEROS_PADDING			:	STD_LOGIC_VECTOR(PADDING_COLUMNS*BITS_FLOAT-1 DOWNTO 0)	:=	(OTHERS	=>	'0');
	CONSTANT	COLUMN_ZEROS_PADDING		:	STD_LOGIC_VECTOR(PADDING2*BITS_FLOAT-1 DOWNTO 0) 			:=	(OTHERS	=>	'0');
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	padding_first_zeros:	FOR i IN 0 TO PADDING2-1	GENERATE -- First rows in zeros
		data_out(PADDING_COLUMNS*BITS_FLOAT*(i+1)-1 DOWNTO PADDING_COLUMNS*BITS_FLOAT*i)	<=	ROW_ZEROS_PADDING;
	END GENERATE;
	
	padding_zeros:	FOR i IN 0 TO ROWS-1	GENERATE -- Columns in zeros
		data_out(PADDING_COLUMNS*BITS_FLOAT*(i+PADDING2+1)-1 DOWNTO PADDING_COLUMNS*BITS_FLOAT*(i+PADDING2))	<=	COLUMN_ZEROS_PADDING & data_in(COLUMNS*BITS_FLOAT*(i+1)-1 DOWNTO COLUMNS*BITS_FLOAT*i) & COLUMN_ZEROS_PADDING;
	END GENERATE;

	padding_last_zeros:	FOR i IN ROWS+PADDING2 TO PADDING_ROWS-1	GENERATE -- Last rows in zeros
		data_out(PADDING_COLUMNS*BITS_FLOAT*(i+1)-1 DOWNTO PADDING_COLUMNS*BITS_FLOAT*i)	<=	ROW_ZEROS_PADDING;
	END GENERATE;
END ARCHITECTURE rtl; -- Architecture