LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;
--------------------------------------------------------------
ENTITY demux_n IS 
	GENERIC(	N			:	INTEGER := 4);
	PORT(		sel		:	IN		STD_LOGIC_VECTOR(N-1 DOWNTO 0);
				f			:	OUT	STD_LOGIC_VECTOR((2**N)-1 DOWNTO 0));
END ENTITY demux_n;
------------------------------------------------------------------
ARCHITECTURE arch_demux_n OF demux_n	IS
BEGIN 
	demux0: FOR i IN 0 TO (2**N)-1 GENERATE	
		f(i)	<= '1'	WHEN sel = STD_LOGIC_VECTOR(to_unsigned(i,N)) ELSE
					'0';
	END GENERATE;
END ARCHITECTURE arch_demux_n;