LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE ieee.numeric_std.all;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY mux_generic_g IS 
	GENERIC(	COLUMNS			:	INTEGER	:=	3;
				ROWS				:	INTEGER	:=	1;
				KERNEL_SIZE		:	INTEGER	:= 3;
				STRIDE			:	INTEGER	:=	1;
				NEW_COLUMNS		:	INTEGER	:=	3;
				NEW_ROWS			:	INTEGER	:=	1;
				SIZE_SELECTOR	:	INTEGER	:=	2); -- Define Generic Parameters
	PORT	(	data_in			:	IN		STD_LOGIC_VECTOR(NEW_ROWS*NEW_COLUMNS*KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
				sel				:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1	DOWNTO 0);
				data_out			:	OUT	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY mux_generic_g; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF mux_generic_g is 
	--SIGNALS
	TYPE		kernel			IS ARRAY(0 TO NEW_ROWS*NEW_COLUMNS-1) OF STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	kernel_mtx		:	kernel;
BEGIN 
	--circuit
	kernel_slice: FOR i IN 0 TO NEW_ROWS*NEW_COLUMNS-1 GENERATE
		kernel_mtx(i)	<=	data_in(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT*(i+1)-1 DOWNTO KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT*i);
	END GENERATE;
	
	data_out	<= kernel_mtx(to_integer(unsigned(sel)));
END ARCHITECTURE rtl; -- Architecture