LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
---------------------- STRIDE = 1 ----------------------
ENTITY data_conv2D IS 
	PORT		(	rst					:	IN		STD_LOGIC;
					clk					:	IN		STD_LOGIC;
					syn_clr				:	IN		STD_LOGIC;
					enable				:	IN		STD_LOGIC;
					padding_sel			:	IN		STD_LOGIC;
					columns_in			:	IN		UNSIGNED(BITS_SETUP_CONV2D	DOWNTO 0);
					rows_in				:	IN		UNSIGNED(BITS_SETUP_CONV2D	DOWNTO 0);
					data_ready			:	OUT	STD_LOGIC;
					data_ready_kernel	:	OUT	STD_LOGIC;
					padding				:	OUT	STD_LOGIC;
					index_out			:	OUT	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
					index_data_k		:	OUT	UNSIGNED(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
					columns_out			:	OUT	UNSIGNED(BITS_SETUP_CONV2D	DOWNTO 0);
					rows_out				:	OUT	UNSIGNED(BITS_SETUP_CONV2D	DOWNTO 0)); -- Define I/O Ports
END ENTITY data_conv2D; -- Entity 
--------------------------------------------------------
ARCHITECTURE name_architecture OF data_conv2D is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT ZEROS_COUNT							:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0)								:=	(OTHERS => '0');
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	column_s,column_next									:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL 	row_s,row_next											:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL 	kernel_column_s,kernel_column_next				:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL 	kernel_row_s,kernel_row_next						:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL 	border_column,border_row							:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	max_tick_column,max_tick_row						:	STD_LOGIC;
	SIGNAL	max_tick_kernel_column,max_tick_kernel_row	:	STD_LOGIC;
	SIGNAL	top_columns,columns									:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	top_rows,rows											:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	SIGNAL	index_out_s												:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1 DOWNTO 0);
	SIGNAL	index_data_k_s											:	UNSIGNED(ADDR_BUFFER_INTERNAL_WIDTH-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	columns		<=	columns_in	WHEN	padding_sel='1'	ELSE
						columns_in-KERNEL_SIZE+1;
	top_columns	<= columns-1;
	rows			<= rows_in		WHEN	padding_sel='1'	ELSE
						rows-KERNEL_SIZE+1;
	top_rows		<=	rows-1;
	--========================================
	--             COUNTER COLUMNS
	--========================================
	max_tick_column			<=	'1'					WHEN	column_s=top_columns																																	ELSE	'0';
	column_next					<=	ZEROS_COUNT			WHEN	syn_clr='1'	OR (max_tick_column='1' AND max_tick_row='1' AND max_tick_kernel_column='1' AND max_tick_kernel_row='1')	ELSE
										column_s+1			WHEN	enable='1' AND max_tick_kernel_column='1'	AND max_tick_kernel_row='1' AND max_tick_row='1'	ELSE
										column_s;

	--========================================
	--              COUNTER ROW
	--========================================
	max_tick_row				<=	'1'					WHEN	row_s=top_rows																																			ELSE	'0';
	row_next						<=	ZEROS_COUNT			WHEN	syn_clr='1'	OR (max_tick_row='1' AND max_tick_kernel_column='1' AND max_tick_kernel_row='1')									ELSE
										row_s+1				WHEN	enable='1' AND max_tick_kernel_column='1'	AND max_tick_kernel_row='1'																ELSE
										row_s;

	--========================================
	--         COUNTER KERNEL COLUMN
	--========================================
	max_tick_kernel_column	<=	'1'					WHEN	kernel_column_s=KERNEL_TOP																															ELSE	'0';
	kernel_column_next		<=	ZEROS_COUNT			WHEN	syn_clr='1'	OR (max_tick_kernel_column='1' AND max_tick_kernel_row='1')																ELSE
										kernel_column_s+1	WHEN	enable='1' AND max_tick_kernel_row='1'																											ELSE
										kernel_column_s;
	
	--========================================
	--          COUNTER KERNEL ROW
	--========================================
	max_tick_kernel_row		<=	'1'					WHEN	kernel_row_s=KERNEL_TOP																																ELSE	'0';
	kernel_row_next			<=	ZEROS_COUNT			WHEN	syn_clr='1'	OR max_tick_kernel_row='1'																											ELSE
										kernel_row_s+1		WHEN	enable='1'																																				ELSE
										kernel_row_s;
	
	--========================================
	--                 OUTPUT
	--========================================
	border_column		<=	column_s+kernel_column_s-PADDING2	WHEN	padding_sel='1'	ELSE
								column_s+kernel_column_s;
	border_row			<=	row_s+kernel_row_s-PADDING2			WHEN	padding_sel='1'	ELSE
								row_s+kernel_row_s;
	index_out_s			<=	border_column*columns+border_row;
	index_data_k_s		<=	kernel_column_s*columns;
	index_out			<= index_out_s(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	index_data_k		<= index_data_k_s(ADDR_BUFFERS_WIDTH-1 DOWNTO 0);
	padding				<=	'1' 	WHEN	(border_column(BITS_SETUP_CONV2D)='1' OR border_column>top_columns OR border_row(BITS_SETUP_CONV2D)='1' OR border_row>top_rows) AND padding_sel='1' ELSE	'0';
	data_ready			<=	max_tick_column AND max_tick_kernel_column AND max_tick_kernel_row AND max_tick_row;
	data_ready_kernel<=	max_tick_kernel_row AND max_tick_kernel_column;
	columns_out			<= columns;
	rows_out				<= rows;
	--========================================
	--                COUNTERS
	--========================================
	counters: PROCESS(clk,rst)
		VARIABLE temp_column				:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
		VARIABLE temp_row					:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
		VARIABLE temp_kernel_column	:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
		VARIABLE temp_kernel_row		:	UNSIGNED(BITS_SETUP_CONV2D DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			temp_column				:=	(OTHERS=>'0');
			temp_row					:=	(OTHERS=>'0');
			temp_kernel_column	:=	(OTHERS=>'0');
			temp_kernel_row		:=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			IF (syn_clr = '1') THEN
				temp_column				:=	(OTHERS=>'0');
				temp_row					:=	(OTHERS=>'0');
				temp_kernel_column	:=	(OTHERS=>'0');
				temp_kernel_row		:=	(OTHERS=>'0');
			ELSE
				temp_column				:=	column_next;
				temp_row					:=	row_next;
				temp_kernel_column	:=	kernel_column_next;
				temp_kernel_row		:=	kernel_row_next;
			END IF;
		END IF;
		column_s				<=	temp_column;
		row_s					<=	temp_row;
		kernel_column_s	<=	temp_kernel_column;
		kernel_row_s		<=	temp_kernel_row;
	END PROCESS;
END ARCHITECTURE name_architecture; -- Architecture