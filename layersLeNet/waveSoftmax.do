onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /softmax_tb/rst_tb
add wave -noupdate /softmax_tb/clk_tb
add wave -noupdate /softmax_tb/syn_clr_tb
add wave -noupdate /softmax_tb/strobe_tb
add wave -noupdate -radix float32 /softmax_tb/data_in_tb
add wave -noupdate -radix unsigned /softmax_tb/data_in_rdaddress_tb
add wave -noupdate -radix float32 /softmax_tb/data_out_tb
add wave -noupdate -radix unsigned /softmax_tb/data_out_wraddress_tb
add wave -noupdate /softmax_tb/data_out_wren_tb
add wave -noupdate /softmax_tb/data_ready_tb
add wave -noupdate /softmax_tb/DUT/rst
add wave -noupdate /softmax_tb/DUT/clk
add wave -noupdate /softmax_tb/DUT/syn_clr
add wave -noupdate /softmax_tb/DUT/strobe
add wave -noupdate -radix float32 /softmax_tb/DUT/data_in
add wave -noupdate -radix unsigned /softmax_tb/DUT/data_in_rdaddress
add wave -noupdate -radix float32 /softmax_tb/DUT/data_out
add wave -noupdate -radix unsigned /softmax_tb/DUT/data_out_wraddress
add wave -noupdate /softmax_tb/DUT/data_out_wren
add wave -noupdate /softmax_tb/DUT/data_ready
add wave -noupdate /softmax_tb/DUT/pr_state
add wave -noupdate /softmax_tb/DUT/nx_state
add wave -noupdate /softmax_tb/DUT/strobe_exp
add wave -noupdate /softmax_tb/DUT/data_ready_exp
add wave -noupdate -radix float32 /softmax_tb/DUT/result_exp
add wave -noupdate /softmax_tb/DUT/set_data_b_acc
add wave -noupdate -radix float32 /softmax_tb/DUT/result_acc
add wave -noupdate /softmax_tb/DUT/data_ready_acc
add wave -noupdate /softmax_tb/DUT/enable_registers_s
add wave -noupdate /softmax_tb/DUT/enable_registers
add wave -noupdate /softmax_tb/DUT/data_exp
add wave -noupdate /softmax_tb/DUT/data_result
add wave -noupdate /softmax_tb/DUT/strobe_div
add wave -noupdate /softmax_tb/DUT/data_ready_div
add wave -noupdate /softmax_tb/DUT/flag_ready
add wave -noupdate /softmax_tb/DUT/ena_sel
add wave -noupdate /softmax_tb/DUT/syn_clr_sel
add wave -noupdate /softmax_tb/DUT/selector
add wave -noupdate /softmax_tb/DUT/selector_s
add wave -noupdate /softmax_tb/DUT/selector_next
add wave -noupdate /softmax_tb/DUT/SELECTOR_ONES
add wave -noupdate /softmax_tb/DUT/SELECTOR_ZEROS_PADDING
add wave -noupdate /softmax_tb/DUT/ONES_VALID
TreeUpdate [SetDefaultTree]
run 3040ns
WaveRestoreCursors {{Cursor 1} {1906881 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 318
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {3192 ns}
