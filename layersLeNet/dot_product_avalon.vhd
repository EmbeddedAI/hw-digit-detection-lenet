LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_product_avalon IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_ADDRESS	:	INTEGER	:=	6); -- Define Generic Parameters
	PORT	(		-- TO BE CONNECTED TO AVALON CLOCK INPUT INTERFACE
					clk				:	IN		STD_LOGIC;
					reset				:	IN		STD_LOGIC;
					-- TO BE CONNECTED TO AVALON MM SLAVE INTERFACE
					s_address		:	IN		STD_LOGIC_VECTOR(SIZE_ADDRESS-1 DOWNTO 0);
					s_chipselect	:	IN		STD_LOGIC;
					s_write			:	IN		STD_LOGIC;
					s_writedata		:	IN		STD_LOGIC_VECTOR(BITS_AVALON-1 DOWNTO 0);
					s_readdata		:	OUT	STD_LOGIC_VECTOR(BITS_AVALON-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY dot_product_avalon; -- Entity 
--------------------------------------------------------
ARCHITECTURE avalonMMslave OF dot_product_avalon is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	ZEROS				:	STD_LOGIC_VECTOR(BITS_AVALON-2 DOWNTO 0) := (OTHERS => '0');
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	s_strobe				:	STD_LOGIC;
	SIGNAL	s_busy				:	STD_LOGIC;
	SIGNAL	s_data_ready		:	STD_LOGIC;
	SIGNAL	s_syn_clr			:	STD_LOGIC;
	SIGNAL	set_syn_clr			:	STD_LOGIC;
	SIGNAL 	set_data_ready		:	STD_LOGIC;
	SIGNAL	clr_data_ready		: 	STD_LOGIC;
	SIGNAL	s_result_dot_reg	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	s_result_dot		:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	wr_en					:	STD_LOGIC;
	SIGNAL	data_en,kernel_en	:	STD_LOGIC;
	SIGNAL	s_address_s			:	STD_LOGIC_VECTOR(SIZE_ADDRESS-1 DOWNTO 0);
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	--========================================
	--              DOT PRODUCT
	--========================================
	dot_product_unit: ENTITY work.dot_product_data
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_ADDRESS) -- generic mapping
	PORT	MAP	(	rst				=>	reset,
						clk				=>	clk,
						syn_clr			=>	s_syn_clr,
						strobe			=>	s_strobe,
						s_address		=>	s_address_s,
						data_en			=> data_en,
						kernel_en		=>	kernel_en,
						s_writedata		=> s_writedata,
						result_dot		=>	s_result_dot,
						data_ready		=>	set_data_ready,
						busy				=>	s_busy); -- port mapping

	--========================================
	--               REGISTERS        
	--========================================						
	PROCESS( clk, reset)
	BEGIN
		IF (reset = '1') THEN
			s_data_ready			<=	'0';
			s_syn_clr				<=	'0';
			s_result_dot_reg		<=	(OTHERS => '0');
		ELSIF(rising_edge(clk)) THEN
			------------------------------------
			IF	(set_syn_clr = '1') THEN
				s_syn_clr			<= s_writedata(0);
			END IF;
			------------------------------------
			IF	(set_data_ready = '1') THEN
				s_data_ready		<= '1';
				s_result_dot_reg	<=	s_result_dot;
			ELSIF (clr_data_ready = '1') THEN
				s_data_ready		<= '0';
				s_result_dot_reg	<=	(OTHERS => '0');
			END IF;
		END IF;
	END PROCESS;
	
	--========================================
	--        WRITE DECODING LOGIC 
	--========================================
	wr_en				<=	'1'	WHEN	(s_write = '1' AND s_chipselect = '1')	ELSE	'0';
	s_strobe			<=	'1'	WHEN	(s_address = "000000" AND wr_en = '1')	ELSE	'0'; -- offset 0 (strobe register)
	set_syn_clr		<=	'1'	WHEN	(s_address = "000001" AND wr_en = '1')	ELSE	'0'; -- offset 1 (sync_clr register)
	clr_data_ready	<=	'1'	WHEN	(s_address = "000100" AND wr_en = '1')	ELSE	'0'; -- offset 4 (clr_data_ready register)
	data_en			<=	'1'	WHEN	(s_address < "011110" AND s_address >="00101" AND wr_en = '1')	ELSE	'0'; -- offset 5-29 (data registers)
	kernel_en		<=	'1'	WHEN	(s_address >="011110" AND wr_en = '1')	ELSE	'0'; -- offset 30-54 (data registers)
	s_address_s		<= STD_LOGIC_VECTOR(UNSIGNED(s_address)-5)	WHEN 	(s_address < "011110" AND s_address >="00101" AND wr_en = '1')	ELSE
							STD_LOGIC_VECTOR(UNSIGNED(s_address)-30)	WHEN 	(s_address >="011110" AND wr_en = '1')	ELSE	
							(OTHERS => '0');

	--========================================
	--  READ MULTIPLEXING LOGIC 
	--========================================
	s_readdata		<=	s_result_dot_reg		WHEN	s_address = "000010"	ELSE	-- offset 2 (result dot register)
							ZEROS & s_busy			WHEN	s_address = "000011"	ELSE	-- offset 3 (s_busy register)
																									--    bit 0: s_busy status
							ZEROS & s_data_ready	WHEN	s_address = "000100"	ELSE	-- offset 4 (s_data_ready register)
							(OTHERS	=>	'0');													--    bit 0: s_data_ready flag
END ARCHITECTURE avalonMMslave; -- Architecture