LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_product_test_tb IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
END ENTITY dot_product_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_dot_product_test OF dot_product_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	rst_tb			:	STD_LOGIC := '1';
	SIGNAL	clk_tb			:	STD_LOGIC := '0';
	SIGNAL	syn_clr_tb		: 	STD_LOGIC;
	SIGNAL	strobe_tb		:	STD_LOGIC;
	SIGNAL	result_dot_tb	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	data_ready_tb	:	STD_LOGIC;
	SIGNAL	busy_tb			:	STD_LOGIC;
BEGIN

	--========================================
	--              RUN 1100ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb <= '0' AFTER 10ns; -- reset is modeled
	clk_tb <= NOT clk_tb AFTER 5ns; -- time is modeled
	syn_clr_tb	<= '0';
	strobe_tb	<=	'0','1' AFTER 20ns,'0' AFTER 30ns;

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.dot_product_test
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	rst				=>	rst_tb,
						clk				=>	clk_tb,
						syn_clr			=>	syn_clr_tb,
						strobe			=>	strobe_tb,
						result_dot		=>	result_dot_tb,
						data_ready		=>	data_ready_tb,
						busy				=>	busy_tb); -- port mapping

END ARCHITECTURE testbench_dot_product_test;