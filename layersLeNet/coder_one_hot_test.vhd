LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY coder_one_hot_test IS 
	GENERIC	(	N			:	INTEGER := 10);
	PORT		(	sel		:	IN		STD_LOGIC_VECTOR(N-1 DOWNTO 0));
END ENTITY coder_one_hot_test; 
--------------------------------------------------------
ARCHITECTURE test_coder_one_hot OF coder_one_hot_test IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	f	:	STD_LOGIC_VECTOR((2**N)-1 DOWNTO 0);
BEGIN

	--========================================
	--        TEST BLOCK INSTANTIATION        
	--=======================================
	code_one_hot: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	N) -- generic mapping
	PORT	MAP	(	sel	=>	sel,
						f		=>	f); -- port mapping

END ARCHITECTURE test_coder_one_hot;