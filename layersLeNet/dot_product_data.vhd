LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_product_data IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
	PORT		(	rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					strobe			:	IN 	STD_LOGIC;
					s_address		:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					data_en			:	IN		STD_LOGIC;
					kernel_en			:	IN		STD_LOGIC;
					s_writedata		:	IN		STD_LOGIC_VECTOR(BITS_AVALON-1 DOWNTO 0);
					result_dot		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_ready		:	OUT 	STD_LOGIC;
					busy				:	OUT 	STD_LOGIC); -- Define I/O Ports
END ENTITY dot_product_data; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_dot_product_data OF dot_product_data is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	slice_mtx		:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	kernel			:	STD_LOGIC_VECTOR(KERNEL_SIZE*KERNEL_SIZE*BITS_FLOAT-1 DOWNTO 0);

BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
--	kernel	<=	"00111111000110011001100110011010001111100100110011001100110011010000000000000000000000000000000000111111000110011001100110011010010000000000011001100110011001100011111111000000000000000000000001000000000000000000000000000000001111111001100110011001100110100000000000000000000000000000000001000000000000000000000000000000001111111000000000000000000000000011111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111111110000000000000000000000001111100100110011001100110011010000000000000000000000000000000000000000000000000000000000000000010000000000011001100110011001100011111110000000000000000000000000111111101001100110011001100110001111111000000000000000000000000011111110000000000000000000000000111111100000000000000000000000";
	
	--========================================
	--              DOT PRODUCT
	--========================================
	dot_product: ENTITY work.dot_product
	GENERIC	MAP(	KERNEL_SIZE		=>	KERNEL_SIZE) -- generic mapping
	PORT	MAP	(	rst				=>	rst,
						clk				=>	clk,
						syn_clr			=>	syn_clr,
						strobe			=>	strobe,
						slice_mtx		=>	slice_mtx,
						kernel			=>	kernel,
						result_dot		=>	result_dot,
						data_ready		=>	data_ready,
						busy				=>	busy); -- port mapping

	--========================================
	--              FLATTEN DATA
	--========================================
	flatten_data: ENTITY work.flatten_data
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_AVALON,
						DATA_QUANTY		=>	KERNEL_SIZE*KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	rst				=>	rst,
						clk				=>	clk,
						syn_clr			=>	syn_clr,
						sel				=>	s_address,
						enable			=>	data_en,
						data_in			=>	s_writedata,
						data_out			=>	slice_mtx); -- port mapping

	--========================================
	--              FLATTEN KERNEL
	--========================================
	kernel_data: ENTITY work.flatten_data
	GENERIC	MAP(	DATA_WIDTH		=>	BITS_AVALON,
						DATA_QUANTY		=>	KERNEL_SIZE*KERNEL_SIZE,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	rst				=>	rst,
						clk				=>	clk,
						syn_clr			=>	syn_clr,
						sel				=>	s_address,
						enable			=>	kernel_en,
						data_in			=>	s_writedata,
						data_out			=>	kernel); -- port mapping

END ARCHITECTURE arch_dot_product_data; -- Architecture