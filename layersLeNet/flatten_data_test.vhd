LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY flatten_data_test IS 
	GENERIC	(	DATA_WIDTH		:	INTEGER	:=	25*BITS_FLOAT;
					DATA_QUANTY		:	INTEGER	:=	20;
					SIZE_SELECTOR	:	INTEGER	:=	9); -- Define Generic Parameters
	PORT	(		rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					selector			:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					enable			:	IN		STD_LOGIC;
					data_in			:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
					check				:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY flatten_data_test; -- Entity 
--------------------------------------------------------
ARCHITECTURE test_flatten_data OF flatten_data_test is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	data_out_s,comparator												:	STD_LOGIC_VECTOR(DATA_QUANTY*DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_0,data_1,data_2,data_3,data_4,data_5						:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_6,data_7,data_8,data_9,data_10,data_11					:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_12,data_13,data_14,data_15,data_16,data_17				:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	data_18,data_19,data_20,data_21,data_22,data_23,data_24	:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);

BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	comparator	<= (OTHERS => '0');--"01000010110001000000000000000000010000101100101000000000000000000100001011010000000000000000000001000010110101100000000000000000010000101101110000000000000000000100001100000000000000000000000001000011000000110000000000000000010000110000011000000000000000000100001100001001000000000000000001000011000011000000000000000000010000110001111000000000000000000100001100100001000000000000000001000011001001000000000000000000010000110010011100000000000000000100001100101010000000000000000001000011001111000000000000000000010000110011111100000000000000000100001101000010000000000000000001000011010001010000000000000000010000110100100000000000000000000100001101011010000000000000000001000011010111010000000000000000010000110110000000000000000000000100001101100011000000000000000001000011011001100000000000000000";
	
	--========================================
	--        TEST BLOCK INSTANTIATION        
	--========================================
	TEST_B:	ENTITY	work.flatten_data
	GENERIC MAP	(	DATA_WIDTH 		=> DATA_WIDTH,
						DATA_QUANTY		=>	DATA_QUANTY,
						SIZE_SELECTOR	=>	SIZE_SELECTOR)
	PORT MAP		(	clk			=>	clk,
						rst			=>	rst,
						syn_clr		=>	syn_clr,
						sel			=>	selector,
						enable		=>	enable,
						data_in		=> data_in,
						data_out		=>	data_out_s);

	--========================================
	--                 OUTPUT
	--========================================						
	check	<=	'1' WHEN	data_out_s=comparator	ELSE	'0';
--	data_0	<=	data_out_s((DATA_WIDTH*1)-1 DOWNTO 0);
--	data_1	<=	data_out_s((DATA_WIDTH*2)-1 DOWNTO DATA_WIDTH*1);
--	data_2	<=	data_out_s((DATA_WIDTH*3)-1 DOWNTO DATA_WIDTH*2);
--	data_3	<=	data_out_s((DATA_WIDTH*4)-1 DOWNTO DATA_WIDTH*3);
--	data_4	<=	data_out_s((DATA_WIDTH*5)-1 DOWNTO DATA_WIDTH*4);
--	data_5	<=	data_out_s((DATA_WIDTH*6)-1 DOWNTO DATA_WIDTH*5);
--	data_6	<=	data_out_s((DATA_WIDTH*7)-1 DOWNTO DATA_WIDTH*6);
--	data_7	<=	data_out_s((DATA_WIDTH*8)-1 DOWNTO DATA_WIDTH*7);
--	data_8	<=	data_out_s((DATA_WIDTH*9)-1 DOWNTO DATA_WIDTH*8);
--	data_9	<=	data_out_s((DATA_WIDTH*10)-1 DOWNTO DATA_WIDTH*9);
--	data_10	<=	data_out_s((DATA_WIDTH*11)-1 DOWNTO DATA_WIDTH*10);
--	data_11	<=	data_out_s((DATA_WIDTH*12)-1 DOWNTO DATA_WIDTH*11);
--	data_12	<=	data_out_s((DATA_WIDTH*13)-1 DOWNTO DATA_WIDTH*12);
--	data_13	<=	data_out_s((DATA_WIDTH*14)-1 DOWNTO DATA_WIDTH*13);
--	data_14	<=	data_out_s((DATA_WIDTH*15)-1 DOWNTO DATA_WIDTH*14);
--	data_15	<=	data_out_s((DATA_WIDTH*16)-1 DOWNTO DATA_WIDTH*15);
--	data_16	<=	data_out_s((DATA_WIDTH*17)-1 DOWNTO DATA_WIDTH*16);
--	data_17	<=	data_out_s((DATA_WIDTH*18)-1 DOWNTO DATA_WIDTH*17);
--	data_18	<=	data_out_s((DATA_WIDTH*19)-1 DOWNTO DATA_WIDTH*18);
--	data_19	<=	data_out_s((DATA_WIDTH*20)-1 DOWNTO DATA_WIDTH*19);
--	data_20	<=	data_out_s((DATA_WIDTH*21)-1 DOWNTO DATA_WIDTH*20);
--	data_21	<=	data_out_s((DATA_WIDTH*22)-1 DOWNTO DATA_WIDTH*21);
--	data_22	<=	data_out_s((DATA_WIDTH*23)-1 DOWNTO DATA_WIDTH*22);
--	data_23	<=	data_out_s((DATA_WIDTH*24)-1 DOWNTO DATA_WIDTH*23);
--	data_24	<=	data_out_s((DATA_WIDTH*25)-1 DOWNTO DATA_WIDTH*24);

END ARCHITECTURE test_flatten_data; -- Architecture