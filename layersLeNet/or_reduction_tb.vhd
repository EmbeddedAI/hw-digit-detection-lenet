LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY or_reduction_tb IS 
	GENERIC	(	MAX_WIDTH	:	INTEGER	:=	5); -- Define Generic Parameters
END ENTITY or_reduction_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_or_reduction OF or_reduction_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	in_vector_tb	:	STD_LOGIC_VECTOR(MAX_WIDTH-1 DOWNTO 0);
	SIGNAL	q_tb				:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 80ns
	--========================================
	or_reduction_testbench_vectors : PROCESS                                                                             
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		in_vector_tb	<=	"00000";
		WAIT FOR 20ns;
		---------------- vector 1 ----------------
		in_vector_tb	<=	"00001";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		in_vector_tb	<=	"01010";
		WAIT FOR 20ns;
		---------------- vector 3 ----------------
		in_vector_tb	<=	"11111";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS or_reduction_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.or_reduction
	GENERIC	MAP(	MAX_WIDTH	=>	MAX_WIDTH) -- generic mapping
	PORT	MAP	(	in_vector	=>	in_vector_tb,
						q				=>	q_tb); -- port mapping

END ARCHITECTURE testbench_or_reduction;