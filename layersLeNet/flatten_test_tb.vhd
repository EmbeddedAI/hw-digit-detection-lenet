LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY flatten_test_tb IS 
	GENERIC	(	CHANNELS	:	INTEGER	:=	50;
					COLUMNS	:	INTEGER	:=	7;
					ROWS		:	INTEGER	:=	7); -- Define Generic Parameters
END ENTITY flatten_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_flatten_test OF flatten_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	enable_tb	:	STD_LOGIC;
	SIGNAL	check_tb		:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 40ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	enable_tb	<=	'0','1' AFTER 20ns;

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.flatten_test
	GENERIC	MAP(	CHANNELS	=>	CHANNELS,
						COLUMNS	=>	COLUMNS,
						ROWS		=>	ROWS) -- generic mapping
	PORT	MAP	(	enable	=> enable_tb,
						check		=>	check_tb); -- port mapping

END ARCHITECTURE testbench_flatten_test;