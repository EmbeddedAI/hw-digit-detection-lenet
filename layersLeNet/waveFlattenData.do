onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /flatten_data_test_tb/DUT/DATA_WIDTH
add wave -noupdate /flatten_data_test_tb/DUT/DATA_QUANTY
add wave -noupdate /flatten_data_test_tb/DUT/SIZE_SELECTOR
add wave -noupdate /flatten_data_test_tb/DUT/rst
add wave -noupdate /flatten_data_test_tb/DUT/clk
add wave -noupdate /flatten_data_test_tb/DUT/syn_clr
add wave -noupdate /flatten_data_test_tb/DUT/selector
add wave -noupdate /flatten_data_test_tb/DUT/enable
add wave -noupdate /flatten_data_test_tb/DUT/data_in
add wave -noupdate /flatten_data_test_tb/DUT/check
add wave -noupdate /flatten_data_test_tb/DUT/data_out_s
add wave -noupdate /flatten_data_test_tb/DUT/comparator
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_0
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_1
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_2
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_3
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_4
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_5
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_6
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_7
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_8
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_9
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_10
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_11
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_12
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_13
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_14
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_15
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_16
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_17
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_18
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_19
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_20
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_21
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_22
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_23
add wave -noupdate -radix float32 /flatten_data_test_tb/DUT/data_24
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {539292 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 398
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {567 ns}
