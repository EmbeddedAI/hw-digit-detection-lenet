LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY flatten_data_test_tb IS 
	GENERIC	(	DATA_WIDTH		:	INTEGER	:=	BITS_FLOAT;
					DATA_QUANTY		:	INTEGER	:=	25;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
END ENTITY flatten_data_test_tb; 
--------------------------------------------------------
ARCHITECTURE testbench_flatten_data_test OF flatten_data_test_tb IS 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	rst_tb		:	STD_LOGIC := '1';
	SIGNAL	clk_tb		:	STD_LOGIC := '0';
	SIGNAL	syn_clr_tb	: 	STD_LOGIC;
	SIGNAL	selector_tb	:	STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
	SIGNAL	enable_tb	:	STD_LOGIC;
	SIGNAL	data_in_tb	:	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
	SIGNAL	check_tb		:	STD_LOGIC;
BEGIN
	--========================================
	--                RUN 540ns
	--========================================
	--========================================
	--            SIGNAL GENERATION
	--========================================
	rst_tb 		<= '0' AFTER 20ns; -- reset is modeled
	clk_tb 		<= NOT clk_tb AFTER 10ns; -- time is modeled
	syn_clr_tb	<=	'0';
	enable_tb	<=	'0','1' AFTER 40ns;

	flatten_data_test_testbench_vectors : PROCESS                                                                                
	BEGIN   
		--========================================
		--              TEST VECTORS
		--========================================
		---------------- vector 0 ----------------
		selector_tb	<=	"00000";
		data_in_tb	<= "01000011011001100000000000000000";
		WAIT FOR 60ns;
		---------------- vector 1 ----------------
		selector_tb	<=	"00001";
		data_in_tb	<= "01000011011000110000000000000000";
		WAIT FOR 20ns;
		---------------- vector 2 ----------------
		selector_tb	<=	"00010";
		data_in_tb	<= "01000011011000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 3 ----------------
		selector_tb	<=	"00011";
		data_in_tb	<= "01000011010111010000000000000000";
		WAIT FOR 20ns;
		---------------- vector 4 ----------------
		selector_tb	<=	"00100";
		data_in_tb	<= "01000011010110100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 5 ----------------
		selector_tb	<=	"00101";
		data_in_tb	<= "01000011010010000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 6 ----------------
		selector_tb	<=	"00110";
		data_in_tb	<= "01000011010001010000000000000000";
		WAIT FOR 20ns;
		---------------- vector 7 ----------------
		selector_tb	<=	"00111";
		data_in_tb	<= "01000011010000100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 8 ----------------
		selector_tb	<=	"01000";
		data_in_tb	<= "01000011001111110000000000000000";
		WAIT FOR 20ns;
		---------------- vector 9 ----------------
		selector_tb	<=	"01001";
		data_in_tb	<= "01000011001111000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 10 ---------------
		selector_tb	<=	"01010";
		data_in_tb	<= "01000011001010100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 11 ---------------
		selector_tb	<=	"01011";
		data_in_tb	<= "01000011001001110000000000000000";
		WAIT FOR 20ns;
		---------------- vector 12 ---------------
		selector_tb	<=	"01100";
		data_in_tb	<= "01000011001001000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 13 ---------------
		selector_tb	<=	"01101";
		data_in_tb	<= "01000011001000010000000000000000";
		WAIT FOR 20ns;
		---------------- vector 14 ---------------
		selector_tb	<=	"01110";
		data_in_tb	<= "01000011000111100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 15 ---------------
		selector_tb	<=	"01111";
		data_in_tb	<= "01000011000011000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 16 ---------------
		selector_tb	<=	"10000";
		data_in_tb	<= "01000011000010010000000000000000";
		WAIT FOR 20ns;
		---------------- vector 17 ---------------
		selector_tb	<=	"10001";
		data_in_tb	<= "01000011000001100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 18 ---------------
		selector_tb	<=	"10010";
		data_in_tb	<= "01000011000000110000000000000000";
		WAIT FOR 20ns;
		---------------- vector 19 ---------------
		selector_tb	<=	"10011";
		data_in_tb	<= "01000011000000000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 20 ---------------
		selector_tb	<=	"10100";
		data_in_tb	<= "01000010110111000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 21 ---------------
		selector_tb	<=	"10101";
		data_in_tb	<= "01000010110101100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 22 ---------------
		selector_tb	<=	"10110";
		data_in_tb	<= "01000010110100000000000000000000";
		WAIT FOR 20ns;
		---------------- vector 23 ---------------
		selector_tb	<=	"10111";
		data_in_tb	<= "01000010110010100000000000000000";
		WAIT FOR 20ns;
		---------------- vector 24 ---------------
		selector_tb	<=	"11000";
		data_in_tb	<= "01000010110001000000000000000000";
		WAIT FOR 20ns;
	WAIT;                                                       
	END PROCESS flatten_data_test_testbench_vectors; -- the process is finished

	--========================================
	--           DUT INSTANTIATION
	--========================================
	DUT: ENTITY work.flatten_data_test
	GENERIC	MAP(	DATA_WIDTH		=>	DATA_WIDTH,
						DATA_QUANTY		=>	DATA_QUANTY,
						SIZE_SELECTOR	=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	rst				=>	rst_tb,
						clk				=>	clk_tb,
						syn_clr			=>	syn_clr_tb,
						selector			=>	selector_tb,
						enable			=>	enable_tb,
						data_in			=>	data_in_tb,
						check				=> check_tb); -- port mapping
	
END ARCHITECTURE testbench_flatten_data_test;