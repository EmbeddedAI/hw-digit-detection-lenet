LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY flatten_data IS 
	GENERIC	(	DATA_WIDTH		:	INTEGER	:=	BITS_FLOAT;
					DATA_QUANTY		:	INTEGER	:=	25;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
	PORT	(		rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					enable			:	IN		STD_LOGIC;
					sel				:	IN		STD_LOGIC_VECTOR(SIZE_SELECTOR-1 DOWNTO 0);
					data_in			:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
					data_out			:	OUT	STD_LOGIC_VECTOR(DATA_QUANTY*DATA_WIDTH-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY flatten_data; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_flatten_data OF flatten_data IS
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	enable_registers_s		:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)-1 DOWNTO 0);
	SIGNAL	enable_registers			:	STD_LOGIC_VECTOR((2**SIZE_SELECTOR)-1 DOWNTO 0);
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	enable_registers((2**SIZE_SELECTOR)-1 DOWNTO DATA_QUANTY) <= (OTHERS => '0');
	
	--========================================
	--             CODER ONE HOT
	--========================================
	code_one_hot: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	SIZE_SELECTOR) -- generic mapping
	PORT	MAP	(	sel	=>	sel,
						f		=>	enable_registers_s); -- port mapping
						
	--========================================
	--            REGISTERS OUTPUT
	--========================================
	output_registers:	FOR i IN 0 TO DATA_QUANTY-1	GENERATE -- Last rows in zeros
		enable_registers(i)	<=	enable_registers_s(i) AND enable;
		my_reg_out_i: ENTITY  work.my_reg
		GENERIC MAP	(	DATA_WIDTH 	=> DATA_WIDTH)
		PORT MAP		(	clk			=>	clk,
							rst			=>	rst,
							ena			=>	enable_registers(i),
							syn_clr		=>	syn_clr,
							d				=>	data_in,
							q				=> data_out(DATA_WIDTH*(i+1)-1 DOWNTO DATA_WIDTH*i));
	END GENERATE;
END ARCHITECTURE arch_flatten_data; -- Architecture