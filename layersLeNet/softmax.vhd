LIBRARY IEEE; -- IEEE library is included
USE IEEE.STD_LOGIC_1164.ALL; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY softmax IS 
	PORT		(	rst						:	IN		STD_LOGIC;
					clk						:	IN		STD_LOGIC;
					syn_clr					:	IN		STD_LOGIC;
					strobe					:	IN		STD_LOGIC;
					data_in					:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
					data_in_rdaddress		:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
					data_out					:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
					data_out_wraddress	:	OUT	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-1	DOWNTO 0);
					data_out_wren			:	OUT	STD_LOGIC;
					data_ready				:	OUT	STD_LOGIC); -- Define I/O Ports
END ENTITY softmax; -- Entity 
--------------------------------------------------------
ARCHITECTURE name_architecture OF softmax is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	SELECTOR_ONES				:	UNSIGNED(NUM_CLASSES_SELECTOR-1 DOWNTO 0)										:=	(OTHERS	=>	'1');
	CONSTANT	SELECTOR_ZEROS_PADDING	:	STD_LOGIC_VECTOR(ADDR_BUFFERS_WIDTH-NUM_CLASSES_SELECTOR-1 DOWNTO 0)	:=	(OTHERS	=>	'0');
	CONSTANT	ONES_VALID					:	STD_LOGIC_VECTOR(NUM_CLASSES-1 DOWNTO 0)										:=	(OTHERS	=>	'1');
	--========================================
	--                 TYPES
	--========================================
	TYPE state IS (idle,start_exp,charge_next_data,operating_exp,operating_acc,start_div,operating_div,charge_data_out,save_data_out,ready);
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL 	pr_state,nx_state			: 	state;
	SIGNAL	strobe_exp					:	STD_LOGIC;
	SIGNAL	data_ready_exp				:	STD_LOGIC;
	SIGNAL	result_exp					: 	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	
	SIGNAL	set_data_b_acc				:	STD_LOGIC;
	SIGNAL	result_acc					: 	STD_LOGIC_VECTOR(BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	data_ready_acc				:	STD_LOGIC;
	
	SIGNAL	sel_exp_div					:	STD_LOGIC;
	SIGNAL	enable_registers_s		:	STD_LOGIC_VECTOR((2**NUM_CLASSES_SELECTOR)-1 DOWNTO 0);
	SIGNAL	enable_registers			:	STD_LOGIC_VECTOR((2**NUM_CLASSES_SELECTOR)-1 DOWNTO 0);
	SIGNAL	data_exp_div_s				:	STD_LOGIC_VECTOR(NUM_CLASSES*BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	data_exp_div				:	STD_LOGIC_VECTOR(NUM_CLASSES*BITS_FLOAT-1	DOWNTO 0);
	SIGNAL	data_result_div			:	STD_LOGIC_VECTOR(NUM_CLASSES*BITS_FLOAT-1	DOWNTO 0);
	
	SIGNAL	strobe_div					:	STD_LOGIC;
	SIGNAL	data_ready_div				:	STD_LOGIC_VECTOR(NUM_CLASSES-1 DOWNTO 0);
	SIGNAL 	flag_ready					:	STD_LOGIC;
	SIGNAL 	ena_sel,syn_clr_sel		:	STD_LOGIC;
	SIGNAL	selector						:	STD_LOGIC_VECTOR(NUM_CLASSES_SELECTOR-1	DOWNTO 0);
	SIGNAL 	selector_s					:	UNSIGNED(NUM_CLASSES_SELECTOR-1 DOWNTO 0);
	SIGNAL	data_address				:	STD_LOGIC_VECTOR(NUM_CLASSES_SELECTOR-1	DOWNTO 0);
	SIGNAL 	data_address_s				:	UNSIGNED(NUM_CLASSES_SELECTOR-1 DOWNTO 0);
	SIGNAL 	selector_next				:	UNSIGNED(NUM_CLASSES_SELECTOR-1 DOWNTO 0);
	
BEGIN
	--========================================
	--                 CIRCUIT
	--========================================
	set_data_b_acc			<=	'0';
	data_in_rdaddress		<=	SELECTOR_ZEROS_PADDING & data_address;
	data_out_wraddress	<=	SELECTOR_ZEROS_PADDING & selector;
	--========================================
	--            EXPONENCIAL FLOAT
	--========================================
	float_exp_e:	 ENTITY work.float_exp
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr,
							strobe		=> strobe_exp,
							data			=> data_in,
							result		=> result_exp,
							data_ready	=> data_ready_exp,
							busy			=> OPEN); -- port mapping

	--========================================
	--              ACCUMULATOR
	--========================================
	acc: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst,
						clk				=> clk,
						syn_clr			=> syn_clr,
						set_data_b		=> set_data_b_acc,
						strobe			=> data_ready_exp,
						data_a			=>	result_exp,
						data_b			=> result_exp,
						result_acc		=> result_acc,
						data_ready		=> data_ready_acc,
						busy				=>	OPEN); -- port mapping
	
	--========================================
	--              SUM SELECTOR
	--========================================
	selector_next	<=	SELECTOR_ONES	WHEN	syn_clr_sel='1'	ELSE
							selector_s+1	WHEN	ena_sel='1'			ELSE
							selector_s;

	--========================================
	--               FLAG READY
	--========================================
	flag_ready		<=	'1'	WHEN selector_s=to_unsigned(NUM_CLASSES-1,NUM_CLASSES_SELECTOR)	ELSE	'0';

	--========================================
	--             CODER ONE HOT
	--========================================
	code_one_hot: ENTITY work.coder_one_hot
	GENERIC	MAP(	N		=>	NUM_CLASSES_SELECTOR) -- generic mapping
	PORT	MAP	(	sel	=>	selector,
						f		=>	enable_registers_s); -- port mapping

	div_reg:	FOR i IN 0 TO NUM_CLASSES-1 GENERATE 
		--========================================
		--            ENABLE REGISTERS
		--========================================
		enable_registers(i)	<=	(enable_registers_s(i) AND data_ready_exp) OR data_ready_div(i);
		--========================================
		--            MUX DATA REGISTER
		--========================================
		mux_i:	ENTITY work.mux2_1
		GENERIC	MAP(	DATA_WIDTH		=>	BITS_FLOAT) -- generic mapping
		PORT	MAP	(	x1					=>	result_exp, -- 0
							x2					=>	data_result_div(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i), -- 1
							sel				=>	sel_exp_div,
							y					=>	data_exp_div_s(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i)); -- port mapping
		--========================================
		--         REGISTERS EXPONENCIAL
		--========================================
		my_reg_i:	ENTITY  work.my_reg
		GENERIC MAP	(	DATA_WIDTH 	=> BITS_FLOAT) -- generic mapping
		PORT MAP		(	clk			=>	clk,
							rst			=>	rst,
							ena			=>	enable_registers(i),
							syn_clr		=>	syn_clr,
							d				=>	data_exp_div_s(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							q				=> data_exp_div(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i));  -- port mapping	
		--========================================
		--              DIVISOR FLOAT
		--========================================
		div_i:	ENTITY work.float_div
		PORT	MAP	(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr,
							strobe		=> strobe_div,
							dataa			=> data_exp_div(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							datab			=> result_acc,
							result		=> data_result_div(BITS_FLOAT*(i+1)-1 DOWNTO BITS_FLOAT*i),
							data_ready	=> data_ready_div(i),
							busy			=> OPEN); -- port mapping	
	END GENERATE;
	
	--===========================================
	--                 MUX OUTPUT
	--===========================================
	mux_out:	ENTITY work.mux_generic
	GENERIC	MAP(	SIZE_SELECTOR	=>	NUM_CLASSES_SELECTOR,
						DATA_WIDTH_IN	=>	NUM_CLASSES*BITS_FLOAT,
						DATA_WIDTH_OUT	=>	BITS_FLOAT) -- generic mapping
	PORT	MAP	(	data_in			=>	data_exp_div,
						sel				=>	selector,
						data_out			=>	data_out); -- port mapping
	
	--===========================================
	--                    FSM
	--===========================================
	------------ Sequential Section -------------
	seq_fsm: PROCESS(clk,rst,data_address_s)
		VARIABLE temp_selector	:	UNSIGNED(NUM_CLASSES_SELECTOR-1 DOWNTO 0);
	BEGIN
		IF (rst = '1') THEN
			pr_state 		<=	idle;
			temp_selector	:=	(OTHERS=>'0');
		ELSIF(rising_edge(clk)) THEN
			pr_state 		<= nx_state;
			temp_selector	:=	selector_next;
		END IF;
		selector			<=	STD_LOGIC_VECTOR(temp_selector);
		selector_s		<=	temp_selector;
		data_address_s	<=	temp_selector+1;
		data_address	<= STD_LOGIC_VECTOR(data_address_s);
	END PROCESS;
	----------- Combinational Section -----------
	comb_fsm: PROCESS (pr_state,strobe,data_ready_exp,flag_ready,data_ready_acc,data_ready_div)
	BEGIN
		CASE pr_state IS
			---------------------------
			WHEN idle =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'1';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				IF (strobe='1') THEN
					nx_state	<= start_exp;
				ELSE
					nx_state	<= idle;
				END IF;
			---------------------------
			WHEN start_exp =>
				strobe_exp		<=	'1';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				nx_state			<= charge_next_data;
			---------------------------
			WHEN charge_next_data =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'1';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				nx_state			<= operating_exp; 
			---------------------------
			WHEN operating_exp =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				IF (data_ready_exp='1' AND flag_ready='1') THEN
					nx_state	<= operating_acc;
				ELSIF (data_ready_exp='1' AND flag_ready='0') THEN
					nx_state	<= start_exp;
				ELSE
					nx_state	<= operating_exp;
				END IF;
			---------------------------
			WHEN operating_acc =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				IF (data_ready_acc='1') THEN
					nx_state	<= start_div;
				ELSE
					nx_state	<= operating_acc;
				END IF;
			---------------------------
			WHEN start_div =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'1';
				sel_exp_div		<=	'1';
				strobe_div		<= '1';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				nx_state			<= operating_div; 
			---------------------------
			WHEN operating_div =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'1';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				IF (data_ready_div=ONES_VALID) THEN
					nx_state	<= charge_data_out;
				ELSE
					nx_state	<= operating_div;
				END IF;
			---------------------------
			WHEN charge_data_out =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'1';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'1';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'0';
				nx_state			<= save_data_out; 
			---------------------------
			WHEN save_data_out =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'1';
				strobe_div		<= '0';
				data_out_wren	<=	'1';
				data_ready		<=	'0';
				IF (flag_ready='1') THEN
					nx_state	<= ready;
				ELSE
					nx_state	<= charge_data_out;
				END IF;
			---------------------------
			WHEN ready =>
				strobe_exp		<=	'0';
				ena_sel      	<=	'0';
				syn_clr_sel		<=	'0';
				sel_exp_div		<=	'0';
				strobe_div		<= '0';
				data_out_wren	<=	'0';
				data_ready		<=	'1';
				nx_state			<= idle; 
			---------------------------
		END CASE;
	END PROCESS;
END ARCHITECTURE name_architecture; -- Architecture